title: Anti-censorship team report: November 2020
---
pub_date: 2020-12-07
---
author: phw
---
tags:

anti-censorship
monthly status
---
categories:

circumvention
reports
---
_html_body:

<p>Tor's anti-censorship team writes monthly reports to keep the world updated on its progress. This blog post summarizes the anti-censorship work we got done in November 2020. Let us know if you have any questions or feedback!</p>
<h2>Snowflake</h2>
<ul>
<li>
<p>Worked on getting Snowflake working for Onion Browser for iOS.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40018">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a><br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40021">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a><br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40023">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Set up and debugged a remote probe test to determine NAT compatability of Snowflakes.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40013">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Modified the NAT type classifications of Snowflake clients to distribute proxies more evenly.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40022">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Created a patch that orders Snowflake's "snowflake-ips" metrics line by the number of requests.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40011">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
</ul>
<h2>Rdsys</h2>
<ul>
<li>
<p>Created a page that shows a bridge's test result. This allows operators to check if their pluggable transports work correctly. You can query your bridge status by visiting:<br />
    <a href="https://bridges.torproject.org/status?id=FINGERPRINT">https://bridges.torproject.org/status?id=FINGERPRINT</a><br />
    Note that the status page currently only tells you the status of your bridge's obfs2, obfs3, obfs4, and scramblesuit.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/10">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/10</a><br />
    Also, the service is still experimental and occasionally offline.</p>
</li>
<li>
<p>Made it possible to look up a bridge's status by providing its hashed fingerprint.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/28">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/28</a></p>
</li>
<li>
<p>Finished documentation on rdsys's design and architecture. You can take a look at it here:<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/blob/master/doc/architecture.md">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/blob/master/d…</a><br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/16">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/16</a></p>
</li>
<li>
<p>Researched libraries to do i18n for rdsys. The library go-i18n seems to check all of our boxes.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/11">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/11</a></p>
</li>
<li>
<p>Filed an issue to get a Transifex resource to do i18n for rdsys.<br />
    <a href="https://gitlab.torproject.org/tpo/community/l10n/-/issues/40009">https://gitlab.torproject.org/tpo/community/l10n/-/issues/40009</a></p>
</li>
<li>
<p>Made rdsys pool bridgestrap requests to make the interaction between both services more efficient.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/24">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/24</a></p>
</li>
<li>
<p>Made rdsys's supported resources configurable. This is important because some bridge operators set up adventurous things like their own meek, and we don't want to distribute those.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/29">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/29</a></p>
</li>
</ul>
<h2>Salmon</h2>
<ul>
<li>Spent some time on our privacy-preserving Salmon modifications but haven't yet managed to come up with a clean implementation. More work is needed.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/21">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/21</a></li>
</ul>
<h2>Bridgestrap</h2>
<ul>
<li>
<p>Made bridgestrap export metrics that are now scraped by our Prometheus instance. The raw metrics are publicly accessible at:<br />
    <a href="https://bridges.torproject.org/bridgestrap-metrics">https://bridges.torproject.org/bridgestrap-metrics</a><br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/4">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/4</a></p>
</li>
<li>
<p>Deployed bridgestrap on polyanthum, the host on which BridgeDB and rdsys run.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/5">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/5</a></p>
</li>
<li>
<p>Finally merged our SETCONF-based rework of how bridgestrap does its testing.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/3">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/3</a></p>
</li>
<li>
<p>Added a field to bridgestrap's test result that informs the requester when a bridge was last tested.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/6">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/6</a></p>
</li>
<li>
<p>Spent some time debugging why the number of functional bridges decreases as we test more bridges in parallel. More work is needed.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/7">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/7</a></p>
</li>
<li>
<p>Made bridgestrap's cache timeout configurable.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/8">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/8</a></p>
</li>
</ul>
<h2>Other</h2>
<ul>
<li>
<p>Cecylia presented the anti-censorship team's yearly progress as part of our State Of The Onion presentation:<br />
    <a href="https://www.youtube.com/watch?v=IyWyTypRGWQ">https://www.youtube.com/watch?v=IyWyTypRGWQ</a></p>
</li>
<li>
<p>Added a new obfs4 default bridge. Thanks to Louis-Philippe Véronneau for operating the bridge!<br />
    <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40212">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/402…</a></p>
</li>
<li>
<p>Brainstormed ways to stream bridge updates from Serge (our bridge authority) to polyanthum (the host where rdsys and bridgestrap are running).<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/9">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap/-/issues/9</a></p>
</li>
<li>
<p>Updated monit configuration to make it monitor Snowflake's probetest service and bridgestrap.</p>
</li>
<li>
<p>Worked with Gus to ask a few folks to run emma in countries that we believe block some aspects of Tor.</p>
</li>
<li>
<p>Philipp is going to review submissions for the DNSPRIVACY 2021 workshop.<br />
    <a href="https://dnspriv21.hotcrp.com">https://dnspriv21.hotcrp.com</a></p>
</li>
<li>
<p>Sponsor 28 scrimmage and PI meeting.</p>
</li>
</ul>

---
_comments:

<a id="comment-290626"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290626" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
    <a href="#comment-290626">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290626" class="permalink" rel="bookmark">Hi,
what is Sponsor 28 about…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
what is Sponsor 28 about? Can you provide more details please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290629"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290629" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290626" class="permalink" rel="bookmark">Hi,
what is Sponsor 28 about…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290629">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290629" class="permalink" rel="bookmark">The project name is …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sponsor 28 is a DARPA-funded project under the title "Reliable Anonymous Communication Evading Censors and Repressors". You can find more information here:<br />
<a href="https://www.darpa.mil/program/resilient-anonymous-communication-for-everyone" rel="nofollow">https://www.darpa.mil/program/resilient-anonymous-communication-for-eve…</a><br />
<a href="https://racecar.cs.georgetown.edu/" rel="nofollow">https://racecar.cs.georgetown.edu/</a><br />
<a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor28" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/sponsors/Sponsor28</a></p>
<p>Sponsor 28 funds most of our Snowflake work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290634"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290634" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290629" class="permalink" rel="bookmark">The project name is …</a> by <a class="tor" title="View user profile." href="/user/46">Philipp Winter</a></p>
    <a href="#comment-290634">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290634" class="permalink" rel="bookmark">Fantastic. Thank you very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fantastic. Thank you very much for the quick reply.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290664" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290629" class="permalink" rel="bookmark">The project name is …</a> by <a class="tor" title="View user profile." href="/user/46">Philipp Winter</a></p>
    <a href="#comment-290664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290664" class="permalink" rel="bookmark">Were there utilitarian…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Were there utilitarian reasons why Tor Project chose that naming convention for sponsors?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290673"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290673" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290664" class="permalink" rel="bookmark">Were there utilitarian…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290673">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290673" class="permalink" rel="bookmark">I believe some sponsors…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe some sponsors occasionally choose to remain anonymous.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290680" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290673" class="permalink" rel="bookmark">I believe some sponsors…</a> by <a class="tor" title="View user profile." href="/user/46">Philipp Winter</a></p>
    <a href="#comment-290680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290680" class="permalink" rel="bookmark">@ Phillip Winter:
I think…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Phillip Winter:</p>
<p>I think you recognize how utterly absurd that sounds.  Once again, as a loyal user and would-be donor (but Tor won't take my money--- why not?), I ask that Tor Project stop taking funds from DARPA and like minded USG agencies which are involved in killing people overseas and doing nasty things inside the USA.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290683"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290683" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290680" class="permalink" rel="bookmark">@ Phillip Winter:
I think…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290683">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290683" class="permalink" rel="bookmark">I don&#039;t think it&#039;s absurd…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think it's absurd. There is a lot of complexity and nuance to the topic of funding. I wouldn't mind having that discussion but it sounds like you've already made up your mind.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290688"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290688" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290680" class="permalink" rel="bookmark">@ Phillip Winter:
I think…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290688">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290688" class="permalink" rel="bookmark">The sponsor number system…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The sponsor number system was developed for consistency across teams, specifically making it easier for accounting. We need to easily and accurately track spending, costs, and activities associated with each sponsor. We often have overlapping projects with the same sponsor and need all teams to be able to easily identify and differentiate each project. The numbers are not meant to anonymize sponsors.</p>
<p>With regard to Philipp's comment, it is not uncommon in the nonprofit world for private foundations to request not to be publicly associated with the projects they fund (in this hypothetical example, they would request not to be listed on <a href="https://torproject.org/about/sponsors" rel="nofollow">https://torproject.org/about/sponsors</a>). This can happen for many reasons, and it does not mean that they are anonymous. If their donation rises above $5,000, we are required by the IRS to report it, and you can find out more by looking at our published tax documentation (<a href="https://www.torproject.org/about/reports/" rel="nofollow">https://www.torproject.org/about/reports/</a>).</p>
<p>As far as I know, there has never been an instance at the Tor Project where funding from public organizations / governments has been anonymous / pseudo anonymous.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-290679"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290679" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>CheckYourMail (not verified)</span> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290629" class="permalink" rel="bookmark">The project name is …</a> by <a class="tor" title="View user profile." href="/user/46">Philipp Winter</a></p>
    <a href="#comment-290679">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290679" class="permalink" rel="bookmark">@ Phillip Winter:
Please…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Phillip Winter:</p>
<p>Please ensure that my post regarding problems with mail delivery Tor Project's P.O. Box appears in this blog.   Your box appears to be defunct (sez USPS) and that is utterly unacceptable for an NGO in the middle of a funding drive.</p>
<p>Quite frustrating that Tor Project continues to take grants from DARPA--- hello? not our friends!---- but is rejecting my own attempts to put my money where my mouth is regarding moving from USG largess to a user-funded model.</p>
<p>TIA</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290681"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290681" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">December 10, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290679" class="permalink" rel="bookmark">@ Phillip Winter:
Please…</a> by <span>CheckYourMail (not verified)</span></p>
    <a href="#comment-290681">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290681" class="permalink" rel="bookmark">We have confirmed that mail…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have confirmed that mail forwarding is in fact active. Whatever issue there is appears to be internal to USPS.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-290627"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290627" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TOR BLOCKED (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
    <a href="#comment-290627">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290627" class="permalink" rel="bookmark">TOR BEING BLOCKED ON ANDROID…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR BEING BLOCKED ON ANDROID VIA H20 ATT MVNO ISP NETWORKS? BRIDGES NOT WORKING...CLOSED.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290630"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290630" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290627" class="permalink" rel="bookmark">TOR BEING BLOCKED ON ANDROID…</a> by <span>TOR BLOCKED (not verified)</span></p>
    <a href="#comment-290630">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290630" class="permalink" rel="bookmark">Are you requesting your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you requesting your bridges over the built-in "request bridges from bridges.torproject.org" feature? If so, that feature broke the other day but we already wrote a fix that will roll out shortly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ode Grille (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
    <a href="#comment-290628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290628" class="permalink" rel="bookmark">Why can I not set TOR as my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why can I not set TOR as my browser by default ? Firefox was set up as such before I joined Tor and it won't let me change that. This seems very abusive to me. I plan to slowly move totally away from Firefox and delete the program altogether. This should solve the problem, shouldn't it ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290642"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290642" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290628" class="permalink" rel="bookmark">Why can I not set TOR as my…</a> by <span>Ode Grille (not verified)</span></p>
    <a href="#comment-290642">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290642" class="permalink" rel="bookmark">https://support.torproject…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://support.torproject.org/tbb/tbb-32/" rel="nofollow">https://support.torproject.org/tbb/tbb-32/</a></p>
<p>Furthermore, some website administrators reject connections from the Tor network. You will likely find that you need to keep a normal browser for certain websites. Remember also that your OS and programs/apps besides Tor Browser will continue to make HTTP connections for other purposes using the configuration and libraries built into your OS which may ignore the tor proxy daemon as well as Tor Browser's strategies to resist fingerprinting. Sometimes, that's nothing to worry about.<br />
<a href="https://support.torproject.org/tbb/tbb-30/" rel="nofollow">https://support.torproject.org/tbb/tbb-30/</a><br />
<a href="https://support.torproject.org/censorship/censorship-2/" rel="nofollow">https://support.torproject.org/censorship/censorship-2/</a><br />
<a href="https://support.torproject.org/tbb/tbb-43/" rel="nofollow">https://support.torproject.org/tbb/tbb-43/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290631"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290631" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
    <a href="#comment-290631">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290631" class="permalink" rel="bookmark">Tor on Android still full of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor on Android still full of bugs and needless permissions, why can't we comment on the post about the latest Android Alpha version?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290636"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290636" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290631" class="permalink" rel="bookmark">Tor on Android still full of…</a> by <span>anonymous (not verified)</span></p>
    <a href="#comment-290636">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290636" class="permalink" rel="bookmark">Some of the Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some of the Android questions have already been answered here: <a href="https://blog.torproject.org/new-release-tor-browser-1004#comments" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-1004#comments</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290637"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290637" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2020</p>
    </div>
    <a href="#comment-290637">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290637" class="permalink" rel="bookmark">You allow my comment which…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You allow my comment which asks questions, but don't bother to respond with an answer despite answering to others? You really need to think of how these things make you look. If I was an agency operating in a hostile enviroment I would remove all Tor on Android installations for security reasons, the dev teams have never historically come to any harm for their failings, its always the end users who pay.</p>
<p>Tor is not free as in freedom, its free as in unmarketable, can you imagine if some VPN company was this shoddy? </p>
<p>Remove needless permissions</p>
<p>Give back about:config</p>
<p>Stop controlling people under the guise of uniformity, anti fingerprinting is less than useless if a real IP can be found due to bugs which are too burdenous for your dev team to look at (a "ticket" doesn't count)</p>
<p>If you truly care for anonymous users and the above is too much then PLEASE hand this branch back to GuardianProject. Orbot and Orfox was years beyond this and yet you've killed off Orfox to ensure the only remaining option is this mess.</p>
<p>Please stop rasing tickets and calling it a day, this isn't how things used to be.</p>
</div>
  </div>
</article>
<!-- Comment END -->
