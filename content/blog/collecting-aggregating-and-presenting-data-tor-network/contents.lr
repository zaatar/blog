title: Collecting, Aggregating, and Presenting Data from The Tor Network
---
pub_date: 2019-04-11
---
author: irl
---
tags:

metrics
collector
---
categories: metrics
---
summary: Tor Metrics consists of several services that work together to collect, aggregate, and present data from the Tor network and related services. We're always looking for ways to improve, and we recently completed a project, the main points of which are included in this post, to document our pipeline and identify areas that could benefit from modernization.
---
_html_body:

<p>As the makers of software dedicated to privacy and freedom online, Tor must take special precautions when collecting data to ensure the privacy of its users while ensuring the integrity of its services.</p>
<p><a href="https://metrics.torproject.org/">Tor Metrics</a> is the central mechanism that the Tor Project uses to evaluate the functionality and ongoing relevance of its technologies. Tor Metrics consists of several services that work together to collect, aggregate, and present data from the Tor network and related services. We're always looking for ways to improve, and we recently completed a project, the main points of which are included in this post, to document our pipeline and identify areas that could benefit from modernization.</p>
<p><img alt="tor-ecosystem" src="/static/images/blog/inline-images/tor-ecosystem.png" width="450" class="align-center" /></p>
<p>As of August 2017, all user-facing Tor Metrics content has moved (back) to the Tor Metrics website. The main reason for gathering everything related to Tor Metrics on a single website is usability. In the background, however, there are <a href="https://metrics.torproject.org/about.html">several services</a> distributed over a dozen hosts that together collect, aggregate, and present the data on the Tor Metrics website. Almost all Tor Metrics codebases are written using Java, although there is also R, SQL and Python. In the future we expect to see more Python code, although Java is still popular with academics and data scientists and we would like to continue supporting easy access to our data for those that want to use Java even if we are using it less ourselves.</p>
<p>Tor relays and bridges collect aggregated statistics about their usage including bandwidth and connecting clients per country. Source aggregation is used to protect the privacy of connecting users—discarding IP addresses and only reporting country information from a local database mapping IP address ranges to countries. These statistics are sent periodically to the directory authorities. <a href="https://metrics.torproject.org/collector.html">CollecTor</a> downloads the latest server descriptors, extra info descriptors containing the aggregated statistics, and consensus documents from the directory authorities and archives them. This archive is public and the metrics-lib Java library can be used to parse the contents of the archive to perform analysis of the data.</p>
<p>In order to provide easy access to visualizations of the historical data archived, the Tor Metrics website contains a number of customizable plots to show <a href="https://metrics.torproject.org/userstats-relay-country.html">user</a>, <a href="https://metrics.torproject.org/bandwidth-flags.html">traffic</a>, <a href="https://metrics.torproject.org/networksize.html">relay</a>, <a href="https://metrics.torproject.org/bridges-ipv6.html">bridge</a>, and <a href="https://metrics.torproject.org/webstats-tb.html">application download</a> statistics over a requested time period and filtered to a particular country. In order to provide easy access to current information about the public Tor network, Onionoo implements a protocol to serve JSON documents over HTTP that can be consumed by applications that would like to display information about relays along with historical bandwidth, uptime, and consensus weight information.</p>
<p>An example of one such application is Relay Search which is used by relay operators, those monitoring the health of the network, and developers of software using the Tor network. Another example of such an application is metrics-bot which posts regular snapshots to <a href="https://twitter.com/toratlas">Twitter</a> and <a href="https://botsin.space/@metricsbot">Mastodon</a> including country statistics and a world map plotting known relays. The majority of our services use metrics-lib to parse the descriptors that have been collected by CollecTor as their source of raw data about the public Tor network.</p>
<p>As the volume of the data we are collecting scales over time, with both the growth of the Tor network and an increase in the number of services providing telemetry and ecosystem health metrics, some of our services become slow or begin to fail as they are no longer fit for purpose. The most critical part of the Tor Metrics pipeline, and the part that has to handle the most data, is CollecTor. In charge of collecting and archiving raw data from the Tor ecosystem, an outage or failure may mean lost data that we can never collect. For this reason we chose CollecTor as the service that we would examine in detail and explore options for modernization using technologies that had not been available previously.</p>
<p>The CollecTor service provides network data collected since 2004, and has existed in its current form as a Java application since 2010. Over time new modules have been added to collect new data and other modules have been retired as the services they downloaded data from no longer exist. As the CollecTor codebase has grown, technical debt has emerged as we have added new features without refactoring existing code. This results in it becoming increasingly difficult to add new data sources to CollecTor as the complexity of the application increases. Some of the requirements of CollecTor, such as concurrency or scheduling, are common to many applications and frameworks exist implementing best practices for these components that could be used in place of the current bespoke implementations.</p>
<p>During the process of examining CollecTor's operation, we discovered some bugs in the Tor directory protocol specification and created fixes for them:</p>
<p>•  The extra-info-digest's sha256-digest isn't actually over the same data as the sha1-digest (#28415)<br />
•  The ns consensus isn't explicitly ns in the network-status-version line of flavored consensuses (#28416)</p>
<p>During the process we also requested some additional functionality from stem that would allow for a Python implementation, which was implemented to support our prototype:</p>
<p>•  Parsing descriptors from a byte-array (#28450)<br />
•  Parsing of detached signatures (#28495)<br />
•  Generating digests for extra-info descriptors, votes, consensuses and microdescriptors (#28398)</p>
<p>A prototype of a "modern CollecTor" was implemented as part of this project. This prototype is known as bushel and the source code and documentation can be found on GitHub at <a href="https://github.com/irl/bushel">https://github.com/irl/bushel</a>. In the future, we hope to build on this work to produce an improved and modernized data pipeline that can better both help our engineering efforts and assist rapid response to attacks or censorship.</p>
<p>For the full details from this project, you can refer to the two technical reports:</p>
<p>Iain R. Learmonth and Karsten Loesing. <a href="https://research.torproject.org/techreports/modern-collector-2018-12-19.pdf">Towards modernising data collection and archive for the Tor network</a>. Technical Report 2018-12-001, The Tor Project, December 2018.</p>
<p>Iain R. Learmonth and Karsten Loesing. <a href="https://research.torproject.org/techreports/metrics-evaluation-2019-03-25.pdf">Tor metrics data collection, aggregation, and presentation</a>. Technical Report 2019-03-001, The Tor Project, March 2019.</p>

---
_comments:

<a id="comment-280776"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280776" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jeff (not verified)</span> said:</p>
      <p class="date-time">April 10, 2019</p>
    </div>
    <a href="#comment-280776">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280776" class="permalink" rel="bookmark">Tor is apparently blocking…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is apparently blocking me from using my debit card online. Is there a fix?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280777"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280777" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 10, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280776" class="permalink" rel="bookmark">Tor is apparently blocking…</a> by <span>jeff (not verified)</span></p>
    <a href="#comment-280777">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280777" class="permalink" rel="bookmark">E-commerce sites and your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>E-commerce sites and your card provider may use "IP reputation databases" to determine when a transaction is at higher risk of being fraudulent. Studies have shown that the number of fraudulent transactions on Tor vs. legitimate ones is roughly the same as for the public Internet, however on the public Internet the transactions will be spread out over millions of IP addresses.</p>
<p>If you see a fraudulent transaction coming from an IP address on the public Internet, you might restrict that IP address from making further transactions in case they are fraudulent too. When you use Tor, you're using a very small pool of IP addresses (those belonging to exit relays). You have millions of users sharing thousands of addresses and the server cannot tell users apart. IP reputation databases weren't designed for this, and so even the low level of fraudulent transactions that occur over Tor (in roughly the same proportion as the public Internet) can cause an IP address to be blocked from performing transactions.</p>
<p>I have personally found that I'm able to conduct online banking via Tor without issues, and even made card payments occasionally without issues. You could try another card provider but in the end it is really up to the e-commerce site or payment processor how they detect fraud.</p>
<p>Our latest statistics on the number of daily users can be found at:</p>
<p><a href="https://metrics.torproject.org/userstats-relay-country.html" rel="nofollow">https://metrics.torproject.org/userstats-relay-country.html</a></p>
<p>Our latest statistics on the number of exit relays can be found at:</p>
<p><a href="https://metrics.torproject.org/relayflags.html" rel="nofollow">https://metrics.torproject.org/relayflags.html</a></p>
<p>The last count is 1952545 users to 845 exit relays, or roughly 2310 users per exit relay.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280778"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280778" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 10, 2019</p>
    </div>
    <a href="#comment-280778">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280778" class="permalink" rel="bookmark">Thanks to the metrics team…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks to the metrics team for your work!  The graphic is very helpful.</p>
<p>The OT thread above is of independent interest and I wish TP had a procedure to encourage more such back and forth on issues encountered by Tor users, which are often not addressed by any documentation easily accessible to the average user.  One possibility would be to start a tradition inspired by Schneier's weekly cat posts in which he encourages readers to ask him anything in the comments; TP could post weekly pictures of the mascot and encourage readers to ask questions they could not get answered elsewhere.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280779"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280779" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 10, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280778" class="permalink" rel="bookmark">Thanks to the metrics team…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280779">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280779" class="permalink" rel="bookmark">I wasn&#039;t aware of those…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wasn't aware of those posts, but it sounds like an interesting approach, I will certainly pass the idea along.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280778" class="permalink" rel="bookmark">Thanks to the metrics team…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280787" class="permalink" rel="bookmark">Also on social media, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Also on social media, but the blog here is convenient in that it doesn't need registration. <strong>We'd like to post on Safest though.</strong></p>
<p>Popular AMA questions can be incorporated into the FAQ afterwards. We'd like to be able to search it by then.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280790"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280790" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280787" class="permalink" rel="bookmark">Also on social media, but…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280790">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280790" class="permalink" rel="bookmark">I discussed this with some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I discussed this with some Tor people yesterday, and it is something we're thinking about doing.</p>
<p>As for the FAQ, we do have the support portal at <a href="https://support.torproject.org/" rel="nofollow">https://support.torproject.org/</a> and it seems that the question in the above thread was <a href="https://support.torproject.org/#tbb-30" rel="nofollow">answered there</a> although differently.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-280786"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280786" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
    <a href="#comment-280786">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280786" class="permalink" rel="bookmark">Housekeeping. Good. Thank…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Housekeeping. Good. Thank you. 2010? Wow.</p>
<p>Please be sure to care for their <a href="https://onion.torproject.org/" rel="nofollow">onion domains</a> too. Is a transition to V3 domains in the pipeline?</p>
<p>What do the colors of the circles on metrics-bot's maps mean?</p>
<p>Why not serve bushel's code on gitweb.torproject.org with the other Tor Projects? Why GitHub, especially now that Microsoft owns it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280788"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280788" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280786" class="permalink" rel="bookmark">Housekeeping. Good. Thank…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280788">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280788" class="permalink" rel="bookmark">As I understand it, we…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As I understand it, we cannot yet transition to v3 onion services due to the lack of OnionBalance support. You can track the progress for that support in our bug tracker.</p>
<p><a href="https://bugs.torproject.org/26768" rel="nofollow">https://bugs.torproject.org/26768</a></p>
<p>With metrics-bot:</p>
<p>* green means Exit<br />
* blue means Guard<br />
* orange means Authority (might be hard to spot this)<br />
* yellow means anything that wasn't an Exit, Guard or Authority</p>
<p>bushel was only a prototype. I have continued to work on it in my free time, but right now I'm the only person working on it as this project has come to an end. The "canonical" repository is the one on my workstation, but if bushel does remain relevant then I will move it to git.torproject.org with a GitHub mirror. GitHub also provides integration with Travis CI which I've been using for testing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280789"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280789" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
    <a href="#comment-280789">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280789" class="permalink" rel="bookmark">Time and time again data…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Time and time again data collection always goes bad !!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280791"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280791" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280789" class="permalink" rel="bookmark">Time and time again data…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280791">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280791" class="permalink" rel="bookmark">Indeed! This is why we put…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Indeed! This is why we put so much effort into making sure our collection of data is safe.</p>
<p>Our original efforts were published in a <a href="https://murdoch.is/papers/wecsr10measuring.pdf" rel="nofollow">peer-reviewed paper</a>. This allowed us to have academics look over the methodologies and point out areas for improvement.</p>
<p>We established a series of principles that ensure that the data collection is safe. These include data minimalisation, source aggregation and transparency of methodologies.</p>
<p>The collection of data helps to improve the safety of the Tor network, by detecting possible censorship events or attacks against the network.</p>
<p>More recently, I have been looking at standardising our safety principles through the Internet Research Task Force to produce an RFC applicable to the wider Internet. You can read a draft text here:</p>
<p><a href="https://datatracker.ietf.org/doc/draft-learmonth-pearg-safe-internet-measurement/" rel="nofollow">https://datatracker.ietf.org/doc/draft-learmonth-pearg-safe-internet-me…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 16, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to irl</p>
    <a href="#comment-280853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280853" class="permalink" rel="bookmark">I just want to say that it&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just want to say that it's so great that you guys reply to the comments in here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280875"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280875" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">April 18, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280853" class="permalink" rel="bookmark">I just want to say that it&#039;s…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280875">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280875" class="permalink" rel="bookmark">Thanks! It&#039;s great that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks! It's great that people had such good questions too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div>
