title: End of Life for Tor 0.2.0.x branch
---
pub_date: 2010-03-31
---
author: phobos
---
tags:

tor 0.2.0
end of life
old-stable
---
categories: releases
---
_html_body:

<p>We have declared end-of-life for Tor 0.2.0.x. Those Tor versions have<br />
several known flaws, and nobody should be using them. You should upgrade.</p>

<p>Specifically, the big flaw in Tor &lt;= 0.2.0.35 is that its list of<br />
directory authorities is out of date, so you'll find it hard to learn<br />
about the network. We're signing the network status consensus with the<br />
old signatures for now, but we're going to stop doing that in a few weeks,<br />
which means your Tor 0.2.0.x will fail to find the current network.</p>

<p>The only exception is people using Debian Lenny -- our nice Debian<br />
packager is trying to keep that package maintained for you.</p>

<p>As a bonus, if you move to a newer Tor you'll get significant performance<br />
boosts as a client, and you'll improve the performance for others as<br />
a relay.</p>

<p>The original message is archived at <a href="http://archives.seul.org/or/announce/Mar-2010/msg00001.html" rel="nofollow">http://archives.seul.org/or/announce/Mar-2010/msg00001.html</a></p>

---
_comments:

<a id="comment-4991"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4991" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2010</p>
    </div>
    <a href="#comment-4991">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4991" class="permalink" rel="bookmark">United Kingdom, Digital</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>United Kingdom, Digital Economy Bill: An ISP perspective<br />
Monday 05 April 2010 09:33:53 by Trefor Davies, CTO of Timico<br />
<a href="http://www.thinkbroadband.com/news/4203-digital-economy-bill-an-isp-perspective.html" rel="nofollow">http://www.thinkbroadband.com/news/4203-digital-economy-bill-an-isp-per…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5149" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2010</p>
    </div>
    <a href="#comment-5149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5149" class="permalink" rel="bookmark">Where can we get more info</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can we get more info on the progress of packaging the new version on Debian?  I look at the PTS but all I see is new packages coming in experimental and unstable all the time.  What needs to be done for them to pass into stable?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5423"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5423" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">April 18, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5149" class="permalink" rel="bookmark">Where can we get more info</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5423">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5423" class="permalink" rel="bookmark">debian stable ships with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>debian stable ships with 0.2.0.x tor, which hit end of life a while ago.  Erinn could answer this better, but I don't believe debian stable will contain 0.2.1 or 0.2.2 until squeeze is released as stable.</p>
<p>Better to use our repositories for current versions of tor for Debian.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5736"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5736" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 22, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-5736">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5736" class="permalink" rel="bookmark">Actually, the Lenny deb is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, the Lenny deb is the one exception to "end of life for 0.2.0.x". The Debian packager for Tor is also a Tor developer, and he's been working hard to keep Tor 0.2.0.x just barely still alive in Lenny.</p>
<p>So while it won't get you the best performance, it should still be safe to use.</p>
<p>If you're running Tor as a relay though, we'd prefer that you use the 0.2.1.x or 0.2.2.x Tor debs. That's because they provide big performance improvements for *other* people on the network.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-5421"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5421" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 18, 2010</p>
    </div>
    <a href="#comment-5421">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5421" class="permalink" rel="bookmark">I use Debian Lenny.  Since</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use Debian Lenny.  Since the big tor update at the time that the problem you mentioned was announced, I noticed that whenever I reboot (my tor client starts by default at boot time) tor seems to be trying to contact all the running Tor servers one by one.  That's supposed to happen, right?</p>
<p>Every now and then I see that my connections while using tor (I have torbutton in Iceweasel) are exiting via an apparently unauthorized tor exit server (not known to sites which maintain a list of known tor exit servers).  Should I be worried about that?</p>
<p>Many thanks for providing tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5737" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 22, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5421" class="permalink" rel="bookmark">I use Debian Lenny.  Since</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5737" class="permalink" rel="bookmark">Yes, your Tor client will</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, your Tor client will keep trying to connect to something until it can establish a connection. When your computer boots the network isn't up yet, and your Tor client thinks either it's just been heavily firewalled, or a bunch of Tor relays are down, or some other catastrophe occurred. Once network shows up, it should settle down.</p>
<p>As for "unauthorized" Tor exit servers, it's much more likely that the websites you use to give you the full list of Tor relays are actually just wrong or out of date.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5755" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 25, 2010</p>
    </div>
    <a href="#comment-5755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5755" class="permalink" rel="bookmark">Has the issue raised in Feb</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Has the issue raised in Feb 2009 by a chinaman I think he is a teacher/prof/educator<br />
regarding the black box/hat effect in tor been solved?</p>
<p>tim</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5757" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 25, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5755" class="permalink" rel="bookmark">Has the issue raised in Feb</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5757" class="permalink" rel="bookmark">To what do you refer?  There</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>To what do you refer?  There is no black box/hat effect in tor that we know of.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
