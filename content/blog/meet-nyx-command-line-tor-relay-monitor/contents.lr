title: Meet Nyx, a Command-Line Tor Relay Monitor
---
pub_date: 2017-11-08
---
author: tommy
---
summary: Never let it be said that we don’t give you a choice of ways to interact with Tor data. This month, Damian Johnson, a long-time Tor volunteer, updated a tool that provides real-time information about your relay. Named for the Greek goddess of night, Nyx is a dead-simple tool with data on bandwidth use, connections, logs, and more.
---
_html_body:

<p> </p>
<p>Never let it be said that we don’t give you a choice of ways to interact with Tor data. This month, Damian Johnson, a long-time Tor volunteer, updated a tool that provides real-time information about your relay. Named for the Greek goddess of night, Nyx is a dead-simple tool with data on bandwidth use, connections, logs, and more.</p>
<p>Relay operators can use Nyx to make sure their relay is functioning properly. At a glance, the tool tells them how much bandwidth the relay is using, whether their CPU is constrained, or if something’s wrong and their relay is failing to establish a connection. In keeping with Tor’s commitment to privacy, Nyx scrubs information about user identities and exit connections.</p>
<p><img alt="A screenshot of Nyx in action." src="/static/images/blog/inline-images/nyx_screenshot.png" class="align-center" /></p>
<h3>The tool formally known as...</h3>
<p>If this all sounds familiar, that’s because this tool used to be called arm. Damian's overhauled the tool, redesigning the tool from the ground up. On his blog, he gives a <a href="http://blog.atagar.com/nyx-release-2-0/">breakdown of what’s new</a>, including:</p>
<ul>
<li>Python 3.x support</li>
<li>Bandwidth graph now prepopulates when you start up, so you have a graph right away</li>
<li>Connections are now available without toggling DisableDebuggerAttachment in your torrc</li>
<li>Support for showing IPv6 connections</li>
</ul>
<p>Check out the <a href="https://nyx.torproject.org">Nyx page</a> to learn more and get the tool.</p>

---
_comments:

<a id="comment-272425"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272425" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Snowden&#039;s Son in Law (not verified)</span> said:</p>
      <p class="date-time">November 08, 2017</p>
    </div>
    <a href="#comment-272425">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272425" class="permalink" rel="bookmark">On his blog, he gives a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p> On his blog, he gives a <a href="http://blog.atagar.com/nyx-release-2-0/" rel="nofollow">breakdown of what’s new</a>, </p></blockquote>
<p>Hmm...  no HTTPS? But Let's Encrypt is free ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272426"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272426" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272425" class="permalink" rel="bookmark">On his blog, he gives a…</a> by <span>Snowden&#039;s Son in Law (not verified)</span></p>
    <a href="#comment-272426">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272426" class="permalink" rel="bookmark">As our noble fellows say …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As our noble fellows say "<em>One SSL certificate a day, keeps the bad guys away...</em>"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>m_m (not verified)</span> said:</p>
      <p class="date-time">November 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272425" class="permalink" rel="bookmark">On his blog, he gives a…</a> by <span>Snowden&#039;s Son in Law (not verified)</span></p>
    <a href="#comment-272430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272430" class="permalink" rel="bookmark">insecure connectionhttp:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>insecure connection<br />
<a href="http://blog.atagar.com/nyx-release-2-0/" rel="nofollow">http://blog.atagar.com/nyx-release-2-0/</a><br />
no https no link !</p>
<p>and what about dns ?<br />
afaik google (and others that i should not name ... ) are not recommended</p>
<p>and what about a contact between relay operator/client ? this tool is like monitoring/reviewing/surveying/analyzing the connection ... so we are un-anonymizing in a way no ?</p>
<p>of course, it is a wonderful tool ... for hackers:testers ... does it help for censoring/shutting down/ remote control ... ?</p>
<p>For specialist only.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272443"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272443" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">November 09, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272430" class="permalink" rel="bookmark">insecure connectionhttp:/…</a> by <span>m_m (not verified)</span></p>
    <a href="#comment-272443">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272443" class="permalink" rel="bookmark">Hi m_m…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi m_m.</p>
<p>&gt; insecure connection</p>
<p>Yup. As mentioned in my other reply I should get around to this. It's an old blog and I've been busy writing Nyx. :P</p>
<p>&gt; and what about dns ?<br />
&gt; afaik google (and others that i should not name ... ) are not recommended</p>
<p>Sorry, not sure I follow this part. What in particular are your DNS concerns?</p>
<p>&gt; and what about a contact between relay operator/client ? this tool is like monitoring/reviewing/surveying/analyzing the connection ... so we are un-anonymizing in a way no ?</p>
<p>Nope. Nyx scrubs any sensitive information (such as exit and client connections). Information it shows is pretty safe.</p>
<p>&gt; of course, it is a wonderful tool ... for hackers:testers ... does it help for censoring/shutting down/ remote control ... ?</p>
<p>Again, not sure I understand the question. You can use Nyx to <a href="https://nyx.torproject.org/images/screenshots/config_editor.png" rel="nofollow">reconfigure Tor</a> if that's your question.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272453"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272453" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nili (not verified)</span> said:</p>
      <p class="date-time">November 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to atagar</p>
    <a href="#comment-272453">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272453" class="permalink" rel="bookmark">&gt; Sorry, not sure I follow…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Sorry, not sure I follow this part. What in particular are your DNS concerns?<br />
does a relay use dns ? does tor use dns ? does a client use dns ?<br />
dns : relays are using dns _ some are not welcome , it is under the responsibility of the relay operator to pick a 'safe' one  _ but how to reject a bad temporary choice as client &amp; as relay operator ; i mean it is not supervised  by something or someone else ; all is based on a relay: tor configuration.<br />
Does it exist a command-line option rejecting/blacklisting a 'not recommended dns' or a general hidden/masked supervisor like a table updated every day/hour ? </p>
<p>&gt; Again, not sure I understand the question. You can use Nyx to reconfigure Tor if that's your question.<br />
do you interact using nyx on the quality of the connection ?<br />
do you interfere in the speed, the bottleneck, the choice of the other relays ?<br />
do you control this part (yours as relay operator) in a way that you could redirect the flux or start/stop a tor instance (located at the home client) on demand ?<br />
i understand nix like a sophisticated security tool and like all these similar tools ; it sounds like a big brother feature.<br />
The client or another control layer should be alerted of a ... connection surveyed by nyx because it is done in real time : too many things are based on trust , in this case the relay operator who could be every one every where ... even the worst.<br />
nix could be improved adding an alert/avert when using a bad dns<br />
nix could be improved adding its own setting/running only in preview not in real time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272460" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">November 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272453" class="permalink" rel="bookmark">&gt; Sorry, not sure I follow…</a> by <span>nili (not verified)</span></p>
    <a href="#comment-272460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272460" class="permalink" rel="bookmark">&gt; does tor use dns ?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; does tor use dns ?</p>
<p>Don't think so. I can't envision any reasons relays would need DNS. As for clients like Tor Browser it of course makes torrified DNS queries when browsing but this has nothing to do with Nyx.</p>
<p>Nyx is a passive monitoring tool. It shouldn't be possible for anything to leak...</p>
<p><a href="https://nyx.torproject.org/#leaking" rel="nofollow">https://nyx.torproject.org/#leaking</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272461"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272461" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>at (not verified)</span> said:</p>
      <p class="date-time">November 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to atagar</p>
    <a href="#comment-272461">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272461" class="permalink" rel="bookmark">thx for your answer , it is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thx for your answer , it is not an intrusive obscure tool but a passive monitoring (no leak) and it helps a lot the  relay operator.<br />
;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-272442"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272442" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">November 09, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272425" class="permalink" rel="bookmark">On his blog, he gives a…</a> by <span>Snowden&#039;s Son in Law (not verified)</span></p>
    <a href="#comment-272442">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272442" class="permalink" rel="bookmark">Sorry about that! That&#039;s an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry about that! That's an old personal blog of mine and you're right that it should use Let's Encrypt. Simply haven't gotten around to it yet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272428"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272428" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>debbbbb (not verified)</span> said:</p>
      <p class="date-time">November 08, 2017</p>
    </div>
    <a href="#comment-272428">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272428" class="permalink" rel="bookmark">Please check this node…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please check this node<br />
137.205.124.35<br />
It always send something.<br />
Tor nodes should detect and disconnect abnormal nodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272455" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>baby (not verified)</span> said:</p>
      <p class="date-time">November 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272428" class="permalink" rel="bookmark">Please check this node…</a> by <span>debbbbb (not verified)</span></p>
    <a href="#comment-272455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272455" class="permalink" rel="bookmark">no, every one can run a node…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no, every one can run a node : you cannot control the others or you are on the other side , a proprietary tool for controlling the others which the consequences are none security , none anonymity.<br />
a false idea (windows/microsoft usa puerile idea ) runs &amp; runs again : secure = controlled = known = nothing to hide = authorized = clean = citizen = vip = masked/opened = white ? innocent ?<br />
the more you are controlled the less you are in security : that is the real life.<br />
* if you detect something abnormal, you can report it and maybe, it will be shut down after a serious investigation. it should be very suspicious to do that automatically. it can be a leak, a hack a bad operator a misconfiguration a bad update an unknown bug or an electric/cable problem ...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272466"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272466" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>neutral (not verified)</span> said:</p>
      <p class="date-time">November 11, 2017</p>
    </div>
    <a href="#comment-272466">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272466" class="permalink" rel="bookmark">what will be the impact on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what will be the impact on Tor Network ?</p>
<p><a href="https://www.battleforthenet.com/" rel="nofollow">https://www.battleforthenet.com/</a></p>
<p>What is net neutrality?<br />
Why does it matter?<br />
*startpage alert</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272483"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272483" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 13, 2017</p>
    </div>
    <a href="#comment-272483">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272483" class="permalink" rel="bookmark">It&#039;s possible to be used, if…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's possible to be used, if of any use, by Tor-Browser-Bundle users?</p>
<p>Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272601"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272601" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">November 17, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272483" class="permalink" rel="bookmark">It&#039;s possible to be used, if…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-272601">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272601" class="permalink" rel="bookmark">Hi. Not entirely sure I grok…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi. Not entirely sure I grok the question but yes, Nyx can be used by Tor Browser users to watch the activity of their tor client.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272692"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272692" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to atagar</p>
    <a href="#comment-272692">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272692" class="permalink" rel="bookmark">Thank you, you got the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, you got the question, I got the answer ;-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273968"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273968" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>realHuman (not verified)</span> said:</p>
      <p class="date-time">February 14, 2018</p>
    </div>
    <a href="#comment-273968">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273968" class="permalink" rel="bookmark">Nyx command show me this:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nyx command show me this: Unable to connect to tor. Maybe it's running without a ControlPort?</p>
<p>what should i do? (im newbie)</p>
</div>
  </div>
</article>
<!-- Comment END -->
