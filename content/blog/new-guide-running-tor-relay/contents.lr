title: The New Guide to Running a Tor Relay
---
pub_date: 2018-02-08
---
author: nusenu-torblog
---
tags:

relays
support
---
categories:

community
relays
---
summary: Have you considered running a relay, but didn't know where to start? You can become one of the many thousands of relay operators powering the Tor network, if you have some basic command-line experience.
---
_html_body:

<p><em>[Update] The Tor Relay Guide is now living in the new <a href="https://community.torproject.org/relay/">Community portal</a>. Did you find a mistake or want to help improving the website? Let us know. Please file your suggestions on <a href="https://gitlab.torproject.org/tpo/web/community/-/issues/">GitLab</a>.</em></p>
<p>Have we told you lately how much we love our relay operators? Relays are the backbone of the Tor network, providing strength and <a href="https://metrics.torproject.org/bandwidth.html">bandwidth</a> for our <a href="https://metrics.torproject.org/userstats-relay-country.html">millions of users</a> worldwide. Without the thousands of fast, reliable relays in the network, Tor wouldn't exist.</p>
<p>Have you considered running a relay, but didn't know where to start? Perhaps you're just looking for a way to help Tor, but you've always thought that running a relay was too complicated or technical for you and the documentation seemed daunting.</p>
<p>We're here to tell you that you can become one of the many thousands of relay operators powering the Tor network, if you have some basic command-line experience.</p>
<p><strong>We've created <a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide">The Tor Relay Guide</a> to:</strong></p>
<ul>
<li>grow the Tor network</li>
<li>demystify relay operation for newcomers</li>
<li>organize important relay resources in one place</li>
<li>encourage everyone who reads it to support the Tor network by setting up their own relay</li>
<li>make the Tor network more robust (example: To reduce the overall fraction of outdated relays, we added instructions for enabling automatic updates.)</li>
<li>emphasize diversity on network and OS level (Most of the Tor network runs on Linux, so we emphasize OS-level diversity and encourage people who can to run BSD-based OSes.)</li>
</ul>
<p>
The guide is split into 3 parts:</p>
<ol>
<li><a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide#Partone:decidingtorunarelay">Deciding to run a Tor relay</a></li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide#Parttwo:technicalsetup">Technical setup</a><br />
    In this section we cover installation steps for the following operating systems:
<ul>
<li>FreeBSD/HardenedBSD</li>
<li>Debian/Ubuntu</li>
<li>CentOS/RHEL</li>
<li>Fedora</li>
<li>openSUSE</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/TorRelayGuide#Partthree:legalinfosocialinfoandmoreresources">Legal info, social info, and more resources</a></li>
</ol>
<figure class="__ns__pop2top" data-fullscreen="false" id="videoContainer"><span class="__NoScript_PlaceHolder__"><button aria-label="Close" class="__NoScript_PlaceHolder__" title="Close" value="close">×</button><a class="__NoScript_PlaceHolder__" href="https://media.torproject.org/video/2012-03-04-BuildingBridges.ogv" style="width: 1200px; height: 644px; position: static; inset: auto; margin: 0px; display: inline-block;" title="&lt;MEDIA&gt;@https://media.torproject.org/video/2012-03-04-BuildingBridges.ogv"><span>&lt;MEDIA&gt;@https://media.torproject.org</span></a></span></figure>
<p>
We've made the guide read-only in order to maintain quality control over the content. This guide updates and replaces existing relay documentation. Eventually, the Tor Relay Guide will become part of our future Community Portal, which will live at community.torproject.org (not available yet). In addition to the Tor Relay Guide content, the Community Portal will include information for Tor trainers, ways to get involved with the Tor community, talks and other events happening in the Tor world, and more.</p>
<p>If you run into technical issues while setting up your relay, please reach out to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays">tor-relays mailing list</a> (subscribing is required to post to the list).</p>
<p>Did you find a mistake or want to help improving the guide? Let us know. Please file your suggestions on <a href="https://trac.torproject.org">trac.torproject.org</a> under the "Community/Relays" component.</p>
<p>Thank you to all who helped with this.</p>
<p> </p>

---
_comments:

<a id="comment-273914"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273914" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 08, 2018</p>
    </div>
    <a href="#comment-273914">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273914" class="permalink" rel="bookmark">The guy that needs a lot of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The guy that needs a lot of our applaud is nusenu who helped a lot with the guide and is always helping out on the tor-relays mailing list, thank you for your service!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273939"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273939" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>portland_f_cking_or (not verified)</span> said:</p>
      <p class="date-time">February 11, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273914" class="permalink" rel="bookmark">The guy that needs a lot of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273939">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273939" class="permalink" rel="bookmark">I think that a recurring…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think that a recurring round of virtual applause, expressions of gratitude, notes of appreciation and all that mushy stuff are long overdue.  thanks guys.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273954"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273954" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273914" class="permalink" rel="bookmark">The guy that needs a lot of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273954">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273954" class="permalink" rel="bookmark">Plus one!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plus one!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273915"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273915" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Arthur Edelstein (not verified)</span> said:</p>
      <p class="date-time">February 08, 2018</p>
    </div>
    <a href="#comment-273915">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273915" class="permalink" rel="bookmark">This new relay guide is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This new relay guide is great. Thanks to all who wrote it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ups (not verified)</span> said:</p>
      <p class="date-time">February 08, 2018</p>
    </div>
    <a href="#comment-273916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273916" class="permalink" rel="bookmark">It&#039;s a hobby/c.v. tag : usa…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's a hobby/c.v. tag : usa old_U style.<br />
Running a relay with the support of an organization/firm/asso/school could be an option, a challenge ; alone it should be a suicide, a waste of time &amp; money.<br />
This article asks participation &amp; effort from "users" or from a politic/social class who could change an influence. They have failed definitively _netneutrality,freedom of speech,etc._ in the usa (canada-australia-newzealand-uk). I think that they should begin to work from NYC to the others towns of the united states and build their own network for their private resident first (melting-pot) : the usa is a materialist world not an idealist one (incompatibility).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273918"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273918" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>aron (not verified)</span> said:</p>
      <p class="date-time">February 08, 2018</p>
    </div>
    <a href="#comment-273918">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273918" class="permalink" rel="bookmark">there is a video in the blog…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>there is a video in the blog post that does nothing with played.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273925"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273925" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nusenu (not verified)</span> said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273918" class="permalink" rel="bookmark">there is a video in the blog…</a> by <span>aron (not verified)</span></p>
    <a href="#comment-273925">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273925" class="permalink" rel="bookmark">I&#039;m sorry if it does not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm sorry if it does not play in your browser, we tested it only in TorBrowser, but you can download it also and play it locally.<br />
Choose your language:<br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-english.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-englis…</a><br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-arabic.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-arabic…</a><br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-farsi.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-farsi…</a><br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-german.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-german…</a><br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-mandarin.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-mandar…</a><br />
<a href="https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-spanish.mp4" rel="nofollow">https://media.torproject.org/video/2012-03-04-BuildingBridges-HD-spanis…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273926" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273918" class="permalink" rel="bookmark">there is a video in the blog…</a> by <span>aron (not verified)</span></p>
    <a href="#comment-273926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273926" class="permalink" rel="bookmark">What browser are you using?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What browser are you using?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273931"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273931" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to steph</p>
    <a href="#comment-273931">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273931" class="permalink" rel="bookmark">Safari Version 11.0.3…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Safari Version 11.0.3 running macOS 10.13.3 (17D47)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273922"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273922" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 08, 2018</p>
    </div>
    <a href="#comment-273922">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273922" class="permalink" rel="bookmark">really great guide! Thanks…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>really great guide! Thanks tor for making all of this possible without being harrassed by the deep state</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273923"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273923" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
    <a href="#comment-273923">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273923" class="permalink" rel="bookmark">Thank you for this! The best…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for this! The best things that I learned there:</p>
<ul>
<li> Do not go to OVH&amp;co to setup your relay, ensure a better diversity ​<a href="https://atlas.torproject.org/#aggregate/as" rel="nofollow">https://atlas.torproject.org/#aggregate/as</a> </li>
<li> Try to make *BSD relays if possible to provide OS diversity, since most relays use Linux.</li>
<li> You can use Ansible ​<a href="https://github.com/nusenu/ansible-relayor" rel="nofollow">https://github.com/nusenu/ansible-relayor</a></li>
</ul>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273930"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273930" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>I_Spy (not verified)</span> said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273923" class="permalink" rel="bookmark">Thank you for this! The best…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273930">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273930" class="permalink" rel="bookmark">Good observation. o/\o</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good observation. o/\o</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273934"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273934" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nin (not verified)</span> said:</p>
      <p class="date-time">February 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273923" class="permalink" rel="bookmark">Thank you for this! The best…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273934">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273934" class="permalink" rel="bookmark">it is a step for spreading…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is a step for spreading the states of the necessity of a private channel.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273942" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2018</p>
    </div>
    <a href="#comment-273942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273942" class="permalink" rel="bookmark">@ nusenu:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ nusenu:</p>
<p>The guide is awesome, and addresses the issues raised many times here (e.g. give us a sense of the minimum useful bw needed for non-exit node) which were not IMO clearly addressed in previous guides.  Also very happy to see good advice regarding improving geolocation diversity.  Thank you much!</p>
<p>Please try to make sure this guide is very easy to find in the torproject.org homepage and that the permalink can be easily cited.  (Having at trac.torproject.org seems a bit awkward, but if there is a good reason for that, I can live with it.)</p>
<p>Quick question:</p>
<p>&gt; Any modern CPU should be fine.</p>
<p>Do the Meltdown/Spectre vulnerabilities pose a potential threat to Tor nodes?  According to what I've read at wired.com etc, these break the low-level distinction between kernel space memory and user space memory, potentially allowing unprivileged process to directly access cryptographic keys etc held in kernel space memory.  If I understand correctly, this could enable a determined attacker to read Tor traffic if they compromise enough nodes.  </p>
<p>Further, I understand that known forensic methods are unlikely to be able to detect that information has been accessed in this way.</p>
<p>Further, I understand that the Intel patches which prevent speculative execution can slow down performance by 30% in such areas as cryptographic processing and data transfers over internet, which would appear to affect Tor nodes.   It appears possible that some software which expects computations to be completed quickly might break due to the slowdown. I guess that most tor servers should handle an unexpected 30% slowdown, but has anyone checked?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273945"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273945" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2018</p>
    </div>
    <a href="#comment-273945">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273945" class="permalink" rel="bookmark">Many thanks to the authors…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Many thanks to the authors of the new Guide!  It clearly addresses many of the basic issues which were not clearly addressed by previous guides (e.g. minimal useful bandwidth, how to increase geolocational diversity).</p>
<p>Can TP please ensure that this new guide remains easy to find/cite?   The URL should be put high on the landing page, I think.</p>
<p>Also, thanks to nusenu for diligently searching for undeclared families which might be doing something bad (e.g. attacking the Tor network as per Carnegie-Mellon/FBI).  That is a very important but tricky task.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273949"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273949" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 12, 2018</p>
    </div>
    <a href="#comment-273949">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273949" class="permalink" rel="bookmark">I certainly hope that TP is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I certainly hope that TP is not simply ignoring FBI's continuing "Going Dark" lobbying of the US Congress:</p>
<p>thehill.com<br />
Week ahead: FBI, intel chiefs head to Capitol Hill<br />
Olivia Beavers<br />
12 Feb 2018</p>
<p>&gt; The head of the FBI is expected to appear before the Senate Intelligence Committee in the coming week for a routine hearing about global threats that pose a risk to U.S. national security.</p>
<p>So FBI Director Wray will be there--- will Tor Project be there to defend Tor users against FBI's latest "Going Dark" demands?  Cannot TP leverage its former status as a product of USG to garner a little face time before Congress, in order to present our POV?</p>
<p>Director Wray is likely to blame Tor for the Dec 2015 cyberattack which brought down portions of the Ukraine power grid, and to use this to argue for making strong encryption (and thus, Tor and Wikipedia) illegal.  It is relevant that the US House just passed a bill funding cybersecurity cooperation between USA and Ukraine, precisely to counter the threat of cyberattacks to power grids, including those using "smart meters" which are far more vulnerable (much higher attack profile).</p>
<p>We need Tor Project to try to make sure that Congress understands what Tor really does and how it works.  We need to make sure Congress understands that Tor is not part of the problem but rather part of the solution.  If we fail to do so, we may wake up to find our doors being kicked in because we are using something we need which has been declared in dead of night to be "illegal encryption".</p>
<p>Neither Wray nor longtime anti-encryption hawk Manhattan DA Cy Vance (son of the former Secretary of State) are likely to mention the fact in their testimony the fact that NYPD has issued encrypted-by-default "smart phones" (iPhone 7) to its officers:</p>
<p>techdirt.com<br />
Will Cy Vance's Anti-Encryption Pitch Change Now That The NYPD's Using iPhones?<br />
from the or-will-encryption-only-be-an-option-for-the-protected-class? dept<br />
12 Feb 2018</p>
<p>It seems noteworthy that two outstanding civil rights figures have died: John Perry Barlow, author of the Internet Manifesto and founder of EFF, and  Asma Jahangir, who fearlessly challenged the worse abuses of the PK government (unfortunately, disappearances of students and bloggers in PK is again rising).</p>
<p><a href="https://en.wikipedia.org/wiki/John_Perry_Barlow" rel="nofollow">https://en.wikipedia.org/wiki/John_Perry_Barlow</a></p>
<p><a href="https://en.wikipedia.org/wiki/A_Declaration_of_the_Independence_of_Cyberspace" rel="nofollow">https://en.wikipedia.org/wiki/A_Declaration_of_the_Independence_of_Cybe…</a></p>
<p><a href="https://en.wikipedia.org/wiki/Asma_Jahangir" rel="nofollow">https://en.wikipedia.org/wiki/Asma_Jahangir</a></p>
<p>So if USG declares unbackdoored encryption illegal, what is our plan?  Will TP renege on its vow never to introduce backdoors into Tor?  Will TP be able to immediately move overseas and to continue to provide Tor to USPERs?  Are TP people prepared to go underground, or will key Tor People employees quit?  Or will TP simply shut down?</p>
<p>I hate the first and last options.  Perhaps if we had an honest public discussion we could find better options.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274013"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274013" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>yetanothermus (not verified)</span> said:</p>
      <p class="date-time">February 18, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273949" class="permalink" rel="bookmark">I certainly hope that TP is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274013">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274013" class="permalink" rel="bookmark">The USA does not rule the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The USA does not rule the world ! They can make Tor illegal as much as they like in their own country, it will still exist.<br />
PGP is illegal but you can still get it easily.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273957"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273957" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ole Tange (not verified)</span> said:</p>
      <p class="date-time">February 13, 2018</p>
    </div>
    <a href="#comment-273957">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273957" class="permalink" rel="bookmark">We are discussing with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We are discussing with libraries to setup exit nodes. This needs to be extremely easy.</p>
<p>So can we get a live-USB where the torrc + this-is-an-exit-node.html can be edited on the USB-disk?</p>
<p>This way they simply need to supply an old server that can boot on USB, and set network using DHCP.</p>
<p>Upgrading will be similarly simple: Send them a new USB-drive. Ask them to turn off the server and change the USB-drive.</p>
<p>To minimize the attack surface it is fine if there are no other services accessible from the internet (e.g. no ssh).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 14, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273957" class="permalink" rel="bookmark">We are discussing with…</a> by <span>Ole Tange (not verified)</span></p>
    <a href="#comment-273973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273973" class="permalink" rel="bookmark">Tails Server is a very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tails Server is a very promising project.  Apparently very hard but Tails Project has been working very hard on this.</p>
<p>I hope that Tails Server will be very useful for things like Library Freedom Project.  Hopefully the first versions will appear soon and LFP can test.</p>
<p><a href="https://tails.boum.org/blueprint/tails_server/" rel="nofollow">https://tails.boum.org/blueprint/tails_server/</a> (added by editor)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273986"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273986" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 14, 2018</p>
    </div>
    <a href="#comment-273986">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273986" class="permalink" rel="bookmark">Here is a good story which…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here is a good story which should help any newbies understand why Tor is so urgently needed by everyone:</p>
<p>theatlantic.com<br />
China's Dystopian Tech Could Be Contagious<br />
Adam Greenfield<br />
14 Feb 2018</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274011"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274011" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Worried1 (not verified)</span> said:</p>
      <p class="date-time">February 18, 2018</p>
    </div>
    <a href="#comment-274011">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274011" class="permalink" rel="bookmark">Teresa May is moving down a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Teresa May is moving down a list, attacking successively smaller messaging platforms.  When will she come to Tor Project?  </p>
<p>theguardian.com<br />
May calls again for tech firms to act on encrypted messaging<br />
Focus shifts to smaller platforms that can ‘quickly become home to criminals and terrorists’<br />
Alex Hern<br />
25 Jan 2018</p>
<p>&gt; “These companies simply cannot stand by while their platforms are used to facilitate child abuse, modern slavery or the spreading of terrorist and extremist content,” she told the audience in Davos. ... “Just as these big companies need to step up, so we also need cross-industry responses because smaller platforms can quickly become home to criminals and terrorists. We have seen that happen with Telegram. And we need to see more cooperation from smaller platforms like this,” she said.</p>
<p>&gt; Despite the years of strong words, however, actions from the UK government have been rare. The Investigatory Powers Act of 2016, strongly backed by May while she was home secretary, gave the government the power to demand the removal of encryption applied to messages, but the government has yet to apply that power to any major technology firm.</p>
<p>TP is neither "major" nor a "firm".  Nor well-funded.  Is TP ready for a removal order issued by the UK government?  What is the plan if you are served with one?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274251" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anonymous (not verified)</span> said:</p>
      <p class="date-time">March 08, 2018</p>
    </div>
    <a href="#comment-274251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274251" class="permalink" rel="bookmark">Why is openvz not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is openvz not recommended?<br />
It's generally cheaper than KVM VPS's.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278304"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278304" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>bsyuagdiua (not verified)</span> said:</p>
      <p class="date-time">October 27, 2018</p>
    </div>
    <a href="#comment-278304">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278304" class="permalink" rel="bookmark">Hope you can give back the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hope you can give back the relay hosting support for Vidalia Bundle</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279438"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279438" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NOYB (not verified)</span> said:</p>
      <p class="date-time">January 19, 2019</p>
    </div>
    <a href="#comment-279438">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279438" class="permalink" rel="bookmark">A stress free method of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A stress free method of setting up a GRSEC hardened relay is here: <a href="https://www.ipfire.org/" rel="nofollow">https://www.ipfire.org/</a></p>
<p>It's maintained. Tor updates are integrated rapidly as released.</p>
<p>The huge spike in traffic I witnessed on 03/01/2018 suggests this box is more survivable than many...."Jus works" and takes a pounding.</p>
</div>
  </div>
</article>
<!-- Comment END -->
