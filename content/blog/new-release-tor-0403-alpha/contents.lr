title: New release: Tor 0.4.0.3-alpha
---
pub_date: 2019-03-22
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.0.3-alpha from the usual place on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release some time in the coming weeks.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.0.3-alpha is the third in its series; it fixes several small bugs from earlier versions.</p>
<h2>Changes in version 0.4.0.3-alpha - 2019-03-22</h2>
<ul>
<li>Minor features (address selection):
<ul>
<li>Treat the subnet 100.64.0.0/10 as public for some purposes; private for others. This subnet is the RFC 6598 (Carrier Grade NAT) IP range, and is deployed by many ISPs as an alternative to RFC 1918 that does not break existing internal networks. Tor now blocks SOCKS and control ports on these addresses and warns users if client ports or ExtORPorts are listening on a RFC 6598 address. Closes ticket <a href="https://bugs.torproject.org/28525">28525</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the March 4 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/29666">29666</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (circuitpadding):
<ul>
<li>Inspect the circuit-level cell queue before sending padding, to avoid sending padding when too much data is queued. Fixes bug <a href="https://bugs.torproject.org/29204">29204</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Correct a misleading error message when IPv4Only or IPv6Only is used but the resolved address can not be interpreted as an address of the specified IP version. Fixes bug <a href="https://bugs.torproject.org/13221">13221</a>; bugfix on 0.2.3.9-alpha. Patch from Kris Katterjohn.</li>
<li>Log the correct port number for listening sockets when "auto" is used to let Tor pick the port number. Previously, port 0 was logged instead of the actual port number. Fixes bug <a href="https://bugs.torproject.org/29144">29144</a>; bugfix on 0.3.5.1-alpha. Patch from Kris Katterjohn.</li>
<li>Stop logging a BUG() warning when Tor is waiting for exit descriptors. Fixes bug <a href="https://bugs.torproject.org/28656">28656</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory management):
<ul>
<li>Refactor the shared random state's memory management so that it actually takes ownership of the shared random value pointers. Fixes bug <a href="https://bugs.torproject.org/29706">29706</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory management, testing):
<ul>
<li>Stop leaking parts of the shared random state in the shared-random unit tests. Fixes bug <a href="https://bugs.torproject.org/29599">29599</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (pluggable transports):
<ul>
<li>Fix an assertion failure crash bug when a pluggable transport is terminated during the bootstrap phase. Fixes bug <a href="https://bugs.torproject.org/29562">29562</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Rust, protover):
<ul>
<li>Add a missing "Padding" value to the Rust implementation of protover. Fixes bug <a href="https://bugs.torproject.org/29631">29631</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (single onion services):
<ul>
<li>Allow connections to single onion services to remain idle without being disconnected. Previously, relays acting as rendezvous points for single onion services were mistakenly closing idle rendezvous circuits after 60 seconds, thinking that they were unused directory-fetching circuits that had served their purpose. Fixes bug <a href="https://bugs.torproject.org/29665">29665</a>; bugfix on 0.2.1.26.</li>
</ul>
</li>
<li>Minor bugfixes (stats):
<ul>
<li>When ExtraInfoStatistics is 0, stop including PaddingStatistics in relay and bridge extra-info documents. Fixes bug <a href="https://bugs.torproject.org/29017">29017</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Downgrade some LOG_ERR messages in the address/* tests to warnings. The LOG_ERR messages were occurring when we had no configured network. We were failing the unit tests, because we backported 28668 to 0.3.5.8, but did not backport 29530. Fixes bug <a href="https://bugs.torproject.org/29530">29530</a>; bugfix on 0.3.5.8.</li>
<li>Fix our gcov wrapper script to look for object files at the correct locations. Fixes bug <a href="https://bugs.torproject.org/29435">29435</a>; bugfix on 0.3.5.1-alpha.</li>
<li>Decrease the false positive rate of stochastic probability distribution tests. Fixes bug <a href="https://bugs.torproject.org/29693">29693</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Windows, CI):
<ul>
<li>Skip the Appveyor 32-bit Windows Server 2016 job, and 64-bit Windows Server 2012 R2 job. The remaining 2 jobs still provide coverage of 64/32-bit, and Windows Server 2016/2012 R2. Also set fast_finish, so failed jobs terminate the build immediately. Fixes bug <a href="https://bugs.torproject.org/29601">29601</a>; bugfix on 0.3.5.4-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-280501"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280501" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Meer (not verified)</span> said:</p>
      <p class="date-time">March 26, 2019</p>
    </div>
    <a href="#comment-280501">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280501" class="permalink" rel="bookmark">How can I download it?
I don…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How can I download it in direct link?<br />
I don't had google account</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280577" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Eerika (not verified)</span> said:</p>
      <p class="date-time">March 28, 2019</p>
    </div>
    <a href="#comment-280577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280577" class="permalink" rel="bookmark">I hope this will be better…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope this will be better than at last time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280800"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280800" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Rahul Chauhan (not verified)</span> said:</p>
      <p class="date-time">April 11, 2019</p>
    </div>
    <a href="#comment-280800">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280800" class="permalink" rel="bookmark">I&#039;m new to this browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm new to this browser</p>
</div>
  </div>
</article>
<!-- Comment END -->
