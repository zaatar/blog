title: New Release: Tor Browser 11.0a8 (Android Only)
---
pub_date: 2021-10-12
---
author: sysrqb
---
tags:

tor-browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a8 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a8 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a8/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable <a href="https://blog.torproject.org/new-release-tor-browser-1058">Windows/macOS/Linux</a> or <a href="https://blog.torproject.org/new-release-tor-browser-1059">Android</a> release instead.</p>
<p>This version is a bugfix for Android.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a7:</a></p>
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40052">Bug 40052</a>: Skip L10nRegistry source registration on Android</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292997"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292997" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2021</p>
    </div>
    <a href="#comment-292997">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292997" class="permalink" rel="bookmark">What risk does this pose?
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What risk does this pose?</p>
<p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1732388" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1732388</a></p>
<p>An answer is needed</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-293108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293108" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">October 25, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292997" class="permalink" rel="bookmark">What risk does this pose?
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-293108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293108" class="permalink" rel="bookmark">There is a risk if it can&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is a risk if it can't be disabled, but Tor Browser will disable it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-293004"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293004" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2021</p>
    </div>
    <a href="#comment-293004">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293004" class="permalink" rel="bookmark">https://bugzilla.mozilla.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=801950" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=801950</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-293006"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293006" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>splendor (not verified)</span> said:</p>
      <p class="date-time">October 16, 2021</p>
    </div>
    <a href="#comment-293006">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293006" class="permalink" rel="bookmark">Referring to not yet…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>Referring to not yet announced &amp; published tor-browser-11.0a9 (win10, en-us, portable install)</strong><br />
"New Idenity"-broom generates message:<br />
<em>Torbutton: Unexpected error during offline cache clearing: Type Error: Service.cache2.appCacheStorage is not a function</em><br />
okaying it, delivers as expected </p>
<p>please fix before publishing</p>
<p>!..sknaht ynam</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-293010"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293010" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2021</p>
    </div>
    <a href="#comment-293010">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293010" class="permalink" rel="bookmark">https://bugzilla.mozilla.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1708830#c1" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1708830#c1</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
