title: New Release: Tor Browser 11.5
---
author: duncan
---
pub_date: 2022-07-14
---
categories:
    
applications
releases
---
summary: Tor Browser 11.5 is now available. This new release builds upon features introduced in Tor Browser 10.5 to transform the user experience of connecting to Tor from heavily censored regions.
---
body:

Tor Browser 11.5 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also from our [distribution directory](https://dist.torproject.org/torbrowser/11.5/). This new release builds upon features introduced in [Tor Browser 10.5](/new-release-tor-browser-105/) to transform the user experience of connecting to Tor from heavily censored regions.

**Update (2022-07-19)**: We're tracking two known issues affecting certain language versions of Tor Browser, and users who attempt to visit IP addresses that do not support HTTPS. Please see **Known issues** below for details.

## What's new?

### Automatic censorship detection and circumvention

We began reshaping the experience of connecting to Tor with the release of [Tor Browser 10.5](/new-release-tor-browser-105/) last year, including the retirement of the Tor Launcher and the integration of the connection flow into the browser window. However, circumventing censorship of the Tor Network itself remained a manual and confusing process – requiring users to dive into Tor Network settings and figure out for themselves how to apply a bridge to unblock Tor. What's more, censorship of Tor isn't uniform – and while a certain pluggable transport or bridge configuration may work in one country, that doesn't mean it'll work elsewhere.

This placed the burden on censored users (who are already under significant pressure) to figure out what option to pick, resulting in a lot of trial, error and frustration in the process. In collaboration with the Anti-Censorship team at the Tor Project, we've sought to reduce this burden with the introduction of **Connection Assist**: a new feature that when required will offer to automatically apply the bridge configuration we think will work best in your location for you.

![Connection assist](connection-assist.gif)

Connection Assist works by looking up and downloading an up-to-date list of country-specific options to try using your location (with your consent). It manages to do so without needing to connect to the Tor Network first by utilizing [moat](https://support.torproject.org/glossary/moat/) – the same domain-fronting tool that Tor Browser uses to request a bridge from torproject.org.

While Connection Assist has reached the milestone of its first stable release, this is only version 1.0, and your feedback will be invaluable to help us improve its user experience in future releases. Users from countries where the Tor Network may be blocked (such as Belarus, China, Russia and Turkmenistan) can test the most recent iteration of this feature by [volunteering as an alpha tester](/vounteer-as-an-alpha-tester/), and [reporting your findings on the Tor forum](https://forum.torproject.net/t/help-us-test-the-new-connection-assist-in-tor-browser-alpha/3654).

### Redesigned Tor Network settings

![Connection settings](connection-settings.png)

We hope that the majority of our users living under extreme censorship will be able to connect to Tor at the press of a button, thanks to Connection Assist. However we know there will always be exceptions to that, and there are many users who prefer to configure their connection manually as well.

That's why we've invested time redesigning Tor Network settings too – featuring:

- **A brand new name:** Tor Network settings is now called Connection settings. This change is intended to clarify exactly what settings you can find within this tab.
- **Connection statuses:** Your last known connection status can now be found at the top of the tab, including the option to test your Internet connection _without_ Tor, using moat, to help you untangle the source of your connection woes.
- **Streamlined bridge options:** Gone is the long list of fields and options. Each method to add a new bridge has been tidied away into individual dialog menus, which will help support further improvements to come.
- **Connection Assist**: When Tor Browser's connection to the Tor Network isn't reachable due to suspected censorship, an additional option to select a bridge automatically becomes available.
- **Brand-new bridge cards:** Bridges used to be almost invisible, even when configured. Now, your saved bridges appear in a handy stack of bridge cards – including new options for sharing bridges too.

![Bridge card diagram](bridge-card-diagram.png)

This is the anatomy of a bridge card when expanded. In addition to copying and sharing the bridge line, each bridge also comes with a unique QR code that will be readable by Tor Browser for Android (and hopefully other Tor-powered apps too) in a future release – helping facilitate the transfer of a working bridge from desktop to mobile.

When you have multiple bridges configured the cards will collapse into a stack – each of which can be expanded again with a click. And when connected, Tor Browser will let you know which bridge it's currently using with the purple "✔ Connected" pill. To help differentiate between your bridges without needing to compare long, unfriendly bridge lines, we've introduced bridge-moji: a short, four emoji visualization you can use to identify the right bridge at a glance.

Lastly, help links within Connection settings now work offline. To recap – there are two types of help links in Tor Browser's settings: those that point to [support.mozilla.org](https://support.mozilla.org), and those that point to [tb-manual.torproject.org](https://tb-manual.torproject.org/) (i.e. the Tor Browser Manual). However, since web-based links aren't very useful when you're troubleshooting connection issues with Tor Browser, the manual is now bundled in Tor Browser 11.5 and is available offline. In addition to the help links within Tor Browser's settings, the manual can be accessed via the Application Menu > Help > Tor Browser Manual, and by entering "about:manual" into your browser's address bar too.

### HTTPS-Only Mode, by default

![HTTPS-Only Mode](https-only.png)

HTTPS-Everywhere is one of two extensions that previously came bundled in Tor Browser, and has led a long and distinguished career protecting our users by automatically upgrading their connections to HTTPS wherever possible. Now, [HTTPS is actually everywhere](https://www.eff.org/deeplinks/2021/09/https-actually-everywhere), and all major web browsers include native support to automatically upgrade to HTTPS. Firefox – the underlying browser on which Tor Browser is based – calls this feature [HTTPS-Only Mode](https://blog.mozilla.org/security/2020/11/17/firefox-83-introduces-https-only-mode/).

Starting in Tor Browser 11.5, HTTPS-Only Mode is enabled by default for desktop, and HTTPS-Everywhere will no longer be bundled with Tor Browser. 

Why now? Research by Mozilla indicates that the [fraction of insecure pages visited by the average users is very low](https://research.mozilla.org/files/2021/03/https_only_upgrading_all_connections_to_https_in_web_browsers.pdf) – limiting the disruption caused to the user experience. Additionally, this change will help protect our users from SSL stripping attacks by [malicious exit relays](/bad-exit-relays-may-june-2020/), and strongly reduces the incentive to spin up exit relays for Man-in-the-Middle attacks in the first place.

You may or may not know that HTTPS-Everywhere also served a second purpose in Tor Browser, and was partly responsible for making [SecureDrop's human-readable onion names](https://securedrop.org/news/introducing-onion-names-securedrop/) work. Well, SecureDrop users can rest assured that we've patched Tor Browser to ensure that human-readable onion names still work in HTTPS-Everywhere's absence.

Note: Unlike desktop, Tor Browser for Android will continue to use HTTPS-Everywhere in the short term. Please see our separate update about Android below.

### Improved font support

![More fonts](improved-fonts.png)

One of Tor Browser's many fingerprinting defenses includes protection against font enumeration – whereby an adversary can fingerprint you using the fonts installed on your system. To counter this, Tor Browser [ships with a standardized bundle of fonts](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/18097) to use in place of those installed on your system. However some writing scripts did not render correctly, while others had no font available in Tor Browser at all.

To solve this issue and expand the number of writing systems supported by Tor Browser, we've bundled many more fonts from the [Noto family](https://fonts.google.com/noto) in this release. Naturally, we have to find a balance between the number of fonts Tor Browser supports without increasing the size of the installer too much, which is something we're very conscious of. So if you spot a language whose characters don't render correctly in Tor Browser, [please let us know](https://support.torproject.org/misc/bug-or-feedback/)!

### Tor Browser for Android

You have no doubt noticed that the features announced above are all for desktop. So, we wanted to share a little update about where we're at with Android:

We know that Tor Browser for Android is quite behind desktop in terms of feature parity. The Tor Project has hit a few bumps in the road over the last couple of years that have delayed our releases, and led us to reassess our roadmap for Android. Since the beginning of the year our priorities for Android have been three-fold:
    
1. Start releasing regular updates for Android again
2. Fix the crashes that many Android users have experienced
3. Begin catching up with Fenix (Firefox for Android) releases

Since then, Android has averaged one stable update per month, crash reports are down significantly thanks to the patch issued in [fenix#40212](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40212), and downloads are working again due to the fixes in [fenix#40192](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40192) and [android-components#40075](https://gitlab.torproject.org/tpo/applications/android-components/-/issues/40075). However we still have work to do to catch up with Fenix, and upgrading Tor Browser to Fenix v102 will be our priority for the next few months.

We've also taken steps to expand the team's capacity in order to dedicate more resources to Android, keep the application stable, and help us bring some of these features described above to Android in the future too.

Thank you for your patience and support!

## Known issues

Tor Browser 11.5 comes with a number of known issues:

#### [Bug torbrowser#40159](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41059): Bridge cards aren't displaying, and toggle themselves off

We're aware of an issue affecting certain language-versions of Tor Browser that's preventing bridge cards from rendering within Connection settings, even if a bridge has been configured. Furthermore, bridges can appear to toggle themselves on and off too.

Our initial testing indicates that this bug is limited to the UI only, and Tor Browser will remain connected to the bridge you have input regardless. We're working on a fix for this issue as a priority.

#### [Bug torbrowser#41050](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41050): "Continue to HTTP Site" button doesn't work on IP addresses

HTTPS-Only Mode will alert you whenever a HTTP connection cannot be upgraded. Normally this alert can by bypassed using the "Continue to HTTP Site" button, which grants an exception for the site in question. However this button does not work when visiting an IP address directly, and an exception cannot be granted by other means.

A fix for this issue will be coming soon. Should you need to visit an IP address over HTTP in the meantime, we recommend against turning HTTPS-Only Mode off. Instead, consider [downgrading temporarily to Tor Browser 11.0.15](https://dist.torproject.org/torbrowser/11.0.15/) until fixed.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/). Thanks to all of the teams across Tor, and the many volunteers, who contributed to this release.

## Full changelog

The full changelog since [Tor Browser 11.0.15](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.5) is:

- All Platforms
  - Update OpenSSL to 1.1.1q
- Windows + OS X + Linux
  - Update Firefox to 91.11.0esr
  - Update Tor-Launcher to 0.2.37
  - Update Translations
  - [Bug tor-browser#11698](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/11698): Incorporate Tor Browser Manual pages into Tor Browser
  - [Bug tor-browser#19850](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/19850): Disable Plaintext HTTP Clearnet Connections

      - [Bug tor-browser#30589](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/30589): Allowed fonts to render a bunch of missing scripts

  - [Bug tor-browser#40458](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40458): Implement about:rulesets https-everywhere replacement
  - [Bug tor-browser-build#40527](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40527): Remove https-everywhere from tor-browser alpha desktop
  - [Bug tor-browser#40562](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40562): Reorganize patchset
  - [Bug tor-browser#40598](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40598): Remove legacy settings read from TorSettings module
  - [Bug tor-browser#40645](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40645): Migrate Moat APIs to Moat.jsm module
  - [Bug tor-browser#40684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40684): Misc UI bug fixes
  - [Bug tor-browser#40773](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40773): Update the about:torconnect frontend page to match additional UI flows
  - [Bug tor-browser#40774](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40774): Update about:preferences page to match new UI designs
  - [Bug tor-browser#40775](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40775): about:ion should not be labeled as a Tor Browser page
  - [Bug tor-browser#40793](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40793): moved Tor configuration options from old-configure.in to moz.configure
  - [Bug tor-browser#40825](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40825): Redirect HTTPS-Only error page when not connected
  - [Bug tor-browser#40912](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40912): Hide screenshots menu since we don't support it
  - [Bug tor-browser#40916](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40916): Remove the browser.download.panel.shown preference
  - [Bug tor-browser#40923](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40923): Consume country code to improve error report
  - [Bug tor-browser#40966](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40966): Render emojis in bridgemoji with SVG files, and added emojii descriptions
  - [Bug tor-browser#41011](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41011): Make sure the Tor Connection status is shown only in about:preferences#connection
  - [Bug tor-browser#41023](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41023): Update manual URLs
  - [Bug tor-browser#41035](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41035): OnionAliasService should use threadsafe ISupports
  - [Bug tor-browser#41036](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41036): Add a preference to disable Onion Aliases
  - [Bug tor-browser#41037](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41037): Fixed the connection preferences on the onboarding
  - [Bug tor-browser#41039](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41039): Set 'startHidden' flag on tor process in tor-launcher
- OS X
  - [Bug tor-browser#40797](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40797): font-family: monospace renders incorrectly on macOS
  - [Bug tor-browser#41004](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41004): Bundled fonts are not picked up on macOS
- Linux
  - [Bug tor-browser#41015](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41015): Add --name parameter to correctly setup WM_CLASS when running as native Wayland client
  - [Bug tor-browser#41043](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41043): Hardcode the UI font on Linux
- Android
  - Update Fenix to 99.0.0b3
- Build System
  - All Platforms
    - [Bug tor-browser-build#40288](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40288): Bump mmdebstrap version to 0.8.6
    - [Bug tor-browser-build#40426](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40426): Update Ubuntu base image to 22.04
    - [Bug tor-browser-build#40519](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40519): Add Alexis' latest PGP key to https-everywhere key ring
  - Android
    - Update Go to 1.18.3
    - [Bug tor-browser-build#40433](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40433): Bump LLVM to 13.0.1 for android builds
    - [Bug tor-browser-build#40470](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40470): Fix zlib build issue for android
    - [Bug tor-browser-build#40485](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40485): Resolve Android reproducibility issues
  - Windows + OS X + Linux
    - [Bug tor-browser-build#34451](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/34451): Include Tor Browser Manual in packages during build
    - [Bug tor-browser-build#40525](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40525): Update the mozconfig for tor-browser-91.9-11.5-2
