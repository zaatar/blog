title: New Release: Tor Browser 9.5a10
---
pub_date: 2020-04-03
---
author: sysrqb
---
tags:

tbb
tor browser
tbb-9.5
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a10 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a10/">distribution directory.</a></p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-908">latest stable release</a> instead.</p>
<p>This release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-11/">security fixes</a> to Firefox.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-9.5a10">changelog</a> since Tor Browser 9.5a9 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Mozilla Bug 1620818 - Release nsDocShell::mContentViewer properly</li>
<li>Mozilla Bug 1626728 - Normalize shutdown</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-287461"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287461" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hhh (not verified)</span> said:</p>
      <p class="date-time">April 07, 2020</p>
    </div>
    <a href="#comment-287461">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287461" class="permalink" rel="bookmark">How to install</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How to install</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287472"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287472" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287461" class="permalink" rel="bookmark">How to install</a> by <span>hhh (not verified)</span></p>
    <a href="#comment-287472">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287472" class="permalink" rel="bookmark">Purple pages at the top -&gt;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Purple pages at the top -&gt; Support -&gt; <a href="https://support.torproject.org/tbb/tbb-46/" rel="nofollow">https://support.torproject.org/tbb/tbb-46/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287463"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287463" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2020</p>
    </div>
    <a href="#comment-287463">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287463" class="permalink" rel="bookmark">You didn&#039;t turn comments on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You didn't turn comments on for the above post for Tails 4.5 so I will comment here.</p>
<p>The link to download tails uses http:</p>
<p><a href="http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5.iso" rel="nofollow">http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5…</a></p>
<p>If changed to https it gives the following warning:</p>
<p>Websites prove their identity via certificates. Tor Browser does not trust this site because it uses a certificate that is not valid for dl.amnesia.boum.org. The certificate is only valid for mirrors.wikimedia.org.</p>
<p>Error code: SSL_ERROR_BAD_CERT_DOMAIN</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287468" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">April 07, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287463" class="permalink" rel="bookmark">You didn&#039;t turn comments on…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287468" class="permalink" rel="bookmark">I&#039;m not sure where you got…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure where you got this http download link. The link on this page is https:<br />
<a href="https://tails.boum.org/install/linux/usb-download/index.en.html" rel="nofollow">https://tails.boum.org/install/linux/usb-download/index.en.html</a></p>
<p>You can contact Tails people about that:<br />
<a href="https://tails.boum.org/support/index.en.html" rel="nofollow">https://tails.boum.org/support/index.en.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-287470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287470" class="permalink" rel="bookmark">The link is also http on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The link is also http on that page:</p>
<p><a href="http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5.img" rel="nofollow">http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5…</a></p>
<p><a href="https://tails.boum.org/install/vm-download/index.en.html:" rel="nofollow">https://tails.boum.org/install/vm-download/index.en.html:</a></p>
<p><a href="http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5.iso" rel="nofollow">http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5…</a></p>
<p>Aren't you the tor project? Can you tell them please.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-287552"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287552" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-287470" class="permalink" rel="bookmark">The link is also http on…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-287552">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287552" class="permalink" rel="bookmark">The ISO doesn&#039;t need https…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The ISO doesn't need https to be verified if you trust and verify the PGP key and signature file. On that page, scroll down to "Verify using OpenPGP". If you trust that one source for the key and if GPG returns "good" when you verify the signature, then the ISO is genuine. If you don't trust that one source, then find more keyservers. Since https is based on preinstalled certificate authority (CA) hierarchies and only represents trust in the connection to a server, you should do this if everything is https too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-287512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-287512" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-287512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-287512" class="permalink" rel="bookmark">The link on that page is not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The link on that page is not https</p>
<p><a href="http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5.img" rel="nofollow">http://dl.amnesia.boum.org/tails/stable/tails-amd64-4.5/tails-amd64-4.5…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
