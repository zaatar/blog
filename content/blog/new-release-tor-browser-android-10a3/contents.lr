title: New Release: Tor Browser for Android 1.0a3
---
pub_date: 2018-10-31
---
author: sysrqb
---
_html_body:

<p> </p>
<p>Tor Browser for Android 1.0a3 is now available from the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha">Tor Browser Project page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/mobile/1.0a3/">distribution directory</a>.</p>
<p>This release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2018-27/">security updates</a> to Firefox.</p>
<p>Moreover, we backport a defense against protocol handler enumeration developed by Mozilla engineers.</p>
<p>Unfortunately, in this release we are temporarily introducing a regression due to a potential proxy-bypass bug within some versions of Android. Tor Browser for Android will not download a website's "favicon" in this release (the small image shown beside the title of the webpage in the list of tabs). From our investigation into this bug, we found Android versions before Android Oreo (<a href="https://en.wikipedia.org/wiki/Android_version_history">Android version 7 and earlier</a>, API level 25 and earlier) leak some information about which webpage the browser is loading. This was corrected in newer versions of Android, however this temporary regression is necessary because it is likely most users have an older version of Android, and there may be other bugs we haven't discovered yet. One bug in the Android networking code is one bug too many. We are working on a new way of downloading these icons.</p>
<p> </p>
<p>The full changelog since Tor Browser for Android 1.0a2 is:</p>
<ul>
<li>Update Firefox to 60.3.0esr</li>
<li>Update Torbutton to 2.1.1</li>
<li>Update HTTPS Everywhere to 2018.9.19</li>
<li>Backport of fixes for bug 1448014, 1458905, 1441345, and 1448305</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/1623">Bug 1623</a>: Block protocol handler enumeration (backport of fix for #680300)</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/28125">Bug 28125</a>: Prevent proxy-bypass bug by Android networking library</li>
</ul>

---
_comments:

<a id="comment-278365"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278365" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2018</p>
    </div>
    <a href="#comment-278365">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278365" class="permalink" rel="bookmark">Such fantastic work, props…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Such fantastic work, props to matthew and igt0 and the rest of the tb and tor core team</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278370"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278370" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2018</p>
    </div>
    <a href="#comment-278370">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278370" class="permalink" rel="bookmark">congratulations on the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congratulations on the release. I wanted to open some bug tickets with the cypherpunks account on track, but the operation is disallowed. The main two issues are I can't download files or save pics and I can't copy-paste text. Android 7</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278378"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278378" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 03, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278370" class="permalink" rel="bookmark">congratulations on the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278378">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278378" class="permalink" rel="bookmark">cypherpunks account has been…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>cypherpunks account has been - for the first time in a heck of a long time - been completely maltreated all because of online fascist tendencies exhibited by teor (a fine and even super tor guy). Now a lot of TB bugs that weren't diagnosed were because of us not bothering to do the testing since cypherpunks - the only multiaccount on trac - being restricted. AH, how much we miss the good old simple times! Arma if you're reading this please take immediate action.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 13, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278378" class="permalink" rel="bookmark">cypherpunks account has been…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278458" class="permalink" rel="bookmark">It is not hard to register…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It is not hard to register in trac. You even don't need to give some email. Just pass CAPTCHA, reload page (relogin), and that's all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-278379"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278379" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 03, 2018</p>
    </div>
    <a href="#comment-278379">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278379" class="permalink" rel="bookmark">There are some big usability…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are some big usability issues with this browser:</p>
<p>- You can't select anything, like text. It's not possible to copy or paste anything.<br />
- Downloads don't work correctly.<br />
- Screenshots are impossible to take, making it necessary to switch to a less secure browser to take a screenshot of a site.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278390"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278390" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">November 04, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278379" class="permalink" rel="bookmark">There are some big usability…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278390">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278390" class="permalink" rel="bookmark">Yes, unfortunately these are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, unfortunately these are known bugs, but thanks for the comment and testing this alpha version.</p>
<p>- You can't select anything, like text. It's not possible to copy or paste anything.<br />
  - <a href="https://trac.torproject.org/projects/tor/ticket/27256" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/27256</a><br />
- Downloads don't work correctly.<br />
  - <a href="https://trac.torproject.org/projects/tor/ticket/27701" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/27701</a><br />
- Screenshots are impossible to take, making it necessary to switch to a less secure browser to take a screenshot of a site.<br />
  - <a href="https://trac.torproject.org/projects/tor/ticket/27987" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/27987</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278401"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278401" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Micio (not verified)</span> said:</p>
      <p class="date-time">November 05, 2018</p>
    </div>
    <a href="#comment-278401">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278401" class="permalink" rel="bookmark">Downloaded from Fdroid right…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Downloaded from Fdroid right now propably 1 hour early ,scripts  are giving me issues, don't  know exactly  what  it means :"A script or more is attempting working not properly " just trying  typeing on tor check on tor project, else an app has stopped to works while I was trying  checking ttb ,very sad .Don't  know how to do solving .Android 5. Anyway thank you  so much for your hard work, I think  you  are  looking for doing all your best.<br />
Best regards your affectionated follower, I really love ttb on android.<br />
Micio.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278402" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>noobiexxx (not verified)</span> said:</p>
      <p class="date-time">November 05, 2018</p>
    </div>
    <a href="#comment-278402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278402" class="permalink" rel="bookmark">i can&#039;t download ascii…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i can't download ascii armored GPG sigs with many Android Firefox versions.<br />
.asc and .sig files must not be formatted (treat them like binary)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278433" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>D-Fr (not verified)</span> said:</p>
      <p class="date-time">November 08, 2018</p>
    </div>
    <a href="#comment-278433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278433" class="permalink" rel="bookmark">I got the browser from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I got the browser from google play last week and it bugged some of the other apps on the phone including Slack, Gmail &amp; Lastpass but may be some more. After the uninstall they all started working. Here some logs<br />
<a href="https://i.postimg.cc/bNWwMMM4/scr.png" rel="nofollow">https://i.postimg.cc/bNWwMMM4/scr.png</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278451"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278451" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>BigBan (not verified)</span> said:</p>
      <p class="date-time">November 12, 2018</p>
    </div>
    <a href="#comment-278451">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278451" class="permalink" rel="bookmark">That would be handy, of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That would be handy, of course, if then in the Stable even orbot falls away. </p>
<p>I am testing the browser just now and so far and it is stable. What bothers me that here similar to Fennec F-Droid once the Pocket recommendations are enabled and actually should not change the settings at TOR. I deactivated them anyway because they annoy me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278648"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278648" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Corey (not verified)</span> said:</p>
      <p class="date-time">December 01, 2018</p>
    </div>
    <a href="#comment-278648">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278648" class="permalink" rel="bookmark">Hey.
Maybe someone can help…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey.<br />
Maybe someone can help. I was using the tor browser alpha and when I went to clear my private data it said that it couldn't remove it all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278659"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278659" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 03, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278648" class="permalink" rel="bookmark">Hey.
Maybe someone can help…</a> by <span>Corey (not verified)</span></p>
    <a href="#comment-278659">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278659" class="permalink" rel="bookmark">What did you try to do …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What did you try to do (which steps allow us to reproduce your problem)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
