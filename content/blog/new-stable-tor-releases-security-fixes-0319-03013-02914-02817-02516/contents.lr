title: New stable Tor releases, with security fixes: 0.3.1.9, 0.3.0.13, 0.2.9.14, 0.2.8.17, 0.2.5.16
---
pub_date: 2017-12-01
---
author: nickm
---
tags:

stable release
security fixes
---
categories: releases
---
summary: There are new stable releases today, fixing the following security issues.  For more information about the issues, follow the links from from https://trac.torproject.org/projects/tor/wiki/TROVE
---
_html_body:

<p>There are new stable releases today, fixing the following security issues.  For more information about the issues, follow the links from from<a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://trac.torproject.org/projects/tor/wiki/TROVE&amp;source=gmail&amp;ust=1512224010614000&amp;usg=AFQjCNF5qNawdA642O5uJs9NHhjSF4Afrw" href="https://trac.torproject.org/projects/tor/wiki/TROVE" rel="noreferrer" target="_blank"> https://trac.torproject.org/<wbr></wbr>projects/tor/wiki/TROVE</a></p>
<p>TROVE-2017-009: Replay-cache ineffective for v2 onion services<br />
TROVE-2017-010: Remote DoS attack against directory authorities<br />
TROVE-2017-011: An attacker can make Tor ask for a password<br />
TROVE-2017-012: Relays can pick themselves in a circuit path<br />
TROVE-2017-013: Use-after-free in onion service v2</p>
<p>You can download the source for 0.3.1.9 from the usual place on the website. For the older release series, see <a href="https://dist.torproject.org/">https://dist.torproject.org/</a>. Binary packages should be available soon. All users should update to one of these releases, or to 0.3.2.6-alpha, also released today.</p>
<p>Below is the changelog for 0.3.1.9.  For the other releases, see the <a href="https://lists.torproject.org/pipermail/tor-announce/2017-December/000147.html">tor-announceme email</a>.</p>
<p>Tor 0.3.1.9 backports important security and stability fixes from the 0.3.2 development series. All Tor users should upgrade to this release, or to another of the releases coming out today.</p>
<h2>Changes in version 0.3.1.9 - 2017-12-01:</h2>
<ul>
<li>Major bugfixes (security, backport from 0.3.2.6-alpha):
<ul>
<li>Fix a denial of service bug where an attacker could use a malformed directory object to cause a Tor instance to pause while OpenSSL would try to read a passphrase from the terminal. (Tor instances run without a terminal, which is the case for most Tor packages, are not impacted.) Fixes bug <a href="https://bugs.torproject.org/24246">24246</a>; bugfix on every version of Tor. Also tracked as TROVE-2017-011 and CVE-2017-8821. Found by OSS-Fuzz as testcase 6360145429790720.</li>
<li>Fix a denial of service issue where an attacker could crash a directory authority using a malformed router descriptor. Fixes bug <a href="https://bugs.torproject.org/24245">24245</a>; bugfix on 0.2.9.4-alpha. Also tracked as TROVE-2017-010 and CVE-2017-8820.</li>
<li>When checking for replays in the INTRODUCE1 cell data for a (legacy) onion service, correctly detect replays in the RSA- encrypted part of the cell. We were previously checking for replays on the entire cell, but those can be circumvented due to the malleability of Tor's legacy hybrid encryption. This fix helps prevent a traffic confirmation attack. Fixes bug <a href="https://bugs.torproject.org/24244">24244</a>; bugfix on 0.2.4.1-alpha. This issue is also tracked as TROVE-2017-009 and CVE-2017-8819.</li>
</ul>
</li>
<li>Major bugfixes (security, onion service v2, backport from 0.3.2.6-alpha):
<ul>
<li>Fix a use-after-free error that could crash v2 Tor onion services when they failed to open circuits while expiring introduction points. Fixes bug <a href="https://bugs.torproject.org/24313">24313</a>; bugfix on 0.2.7.2-alpha. This issue is also tracked as TROVE-2017-013 and CVE-2017-8823.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (security, relay, backport from 0.3.2.6-alpha):
<ul>
<li>When running as a relay, make sure that we never build a path through ourselves, even in the case where we have somehow lost the version of our descriptor appearing in the consensus. Fixes part of bug <a href="https://bugs.torproject.org/21534">21534</a>; bugfix on 0.2.0.1-alpha. This issue is also tracked as TROVE-2017-012 and CVE-2017-8822.</li>
<li>When running as a relay, make sure that we never choose ourselves as a guard. Fixes part of bug <a href="https://bugs.torproject.org/21534">21534</a>; bugfix on 0.3.0.1-alpha. This issue is also tracked as TROVE-2017-012 and CVE-2017-8822.</li>
</ul>
</li>
<li>Major bugfixes (exit relays, DNS, backport from 0.3.2.4-alpha):
<ul>
<li>Fix an issue causing DNS to fail on high-bandwidth exit nodes, making them nearly unusable. Fixes bugs 21394 and 18580; bugfix on 0.1.2.2-alpha, which introduced eventdns. Thanks to Dhalgren for identifying and finding a workaround to this bug and to Moritz, Arthur Edelstein, and Roger for helping to track it down and analyze it.</li>
</ul>
</li>
<li>Minor features (bridge):
<ul>
<li>Bridges now include notice in their descriptors that they are bridges, and notice of their distribution status, based on their publication settings. Implements ticket <a href="https://bugs.torproject.org/18329">18329</a>. For more fine- grained control of how a bridge is distributed, upgrade to 0.3.2.x or later.</li>
</ul>
</li>
<li>Minor features (directory authority, backport from 0.3.2.6-alpha):
<ul>
<li>Add an IPv6 address for the "bastet" directory authority. Closes ticket <a href="https://bugs.torproject.org/24394">24394</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfix (relay address resolution, backport from 0.3.2.1-alpha):
<ul>
<li>Avoid unnecessary calls to directory_fetches_from_authorities() on relays, to prevent spurious address resolutions and descriptor rebuilds. This is a mitigation for bug <a href="https://bugs.torproject.org/21789">21789</a>. Fixes bug <a href="https://bugs.torproject.org/23470">23470</a>; bugfix on in 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.2.1-alpha):
<ul>
<li>Fix unused variable warnings in donna's Curve25519 SSE2 code. Fixes bug <a href="https://bugs.torproject.org/22895">22895</a>; bugfix on 0.2.7.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay shutdown, annoyance, backport from 0.3.2.2-alpha):
<ul>
<li>When a circuit is marked for close, do not attempt to package any cells for channels on that circuit. Previously, we would detect this condition lower in the call stack, when we noticed that the circuit had no attached channel, and log an annoying message. Fixes bug <a href="https://bugs.torproject.org/8185">8185</a>; bugfix on 0.2.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service, backport from 0.3.2.5-alpha):
<ul>
<li>Rename the consensus parameter "hsdir-interval" to "hsdir_interval" so it matches dir-spec.txt. Fixes bug <a href="https://bugs.torproject.org/24262">24262</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay, crash, backport from 0.3.2.4-alpha):
<ul>
<li>Avoid a crash when transitioning from client mode to bridge mode. Previously, we would launch the worker threads whenever our "public server" mode changed, but not when our "server" mode changed. Fixes bug <a href="https://bugs.torproject.org/23693">23693</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
</ul>

