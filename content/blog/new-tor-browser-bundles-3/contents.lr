title: New Tor Browser Bundles
---
pub_date: 2011-07-14
---
author: erinn
---
tags:

release candidate
firefox updates
tbb
---
categories:

applications
releases
---
_html_body:

<p>All of the alpha Tor Browser Bundles have been updated to the latest Tor<br />
0.2.2.30-rc, and the Windows stable bundle has been updated with the latest<br />
Firefox 3.6.19.</p>

<p>The experimental bundles also now contain Firefox 5 and Polipo has been removed<br />
from all of them.</p>

<p><a href="https://torproject.org/download/download" rel="nofollow">https://torproject.org/download/download</a></p>

<p><strong>Firefox 3.6 Tor Browser Bundles</strong></p>

<p><strong>Windows bundle</strong><br />
<strong>1.3.26:  Released 2011-07-13</strong></p>

<ul>
<li>Update Firefox to 3.6.19</li>
<li>Update HTTPS-Everywhere to 1.0.0development.4</li>
<li>Update Libevent to 2.0.12-stable</li>
</ul>

<p><strong>OS X bundle</strong><br />
<strong>1.1.22:  Released 2011-07-13</strong></p>

<ul>
<li>Update Tor to 0.2.2.30-rc</li>
<li>Update Firefox to 3.6.19</li>
<li>Update HTTPS-Everywhere to 1.0.0development.4</li>
<li>Update NoScript to 2.1.1.2</li>
</ul>

<p><strong>Linux bundles</strong><br />
<strong>1.1.12: Released 2011-07-13</strong></p>

<ul>
<li>Update Tor to 0.2.2.30-rc</li>
<li>Update Firefox to 3.6.19</li>
<li>Update HTTPS-Everywhere to 1.0.0development.4</li>
<li>Update NoScript to 2.1.1.2</li>
</ul>

<p><strong>Firefox 4 Tor Browser Bundles</strong></p>

<p><strong>Tor Browser Bundle (2.2.30-1)</strong></p>

<ul>
<li>Update Tor to 0.2.2.30-rc</li>
<li>Update Firefox to 5.0.1</li>
<li>Update Torbutton to 1.4.0</li>
<li>Update Libevent to 2.0.12-stable</li>
<li>Update HTTPS-Everywhere to 1.0.0development.4</li>
<li>Update NoScript to 2.1.1.2</li>
</ul>

<p>Temporary direct download links for Firefox 5 bundles:</p>

<ul>
<li><a href="https://torproject.org/dist/torbrowser/tor-browser-2.2.30-2-alpha_en-US.exe" rel="nofollow">Windows Tor Browser Bundle with Firefox 5</a> (<a href="https://torproject.org/dist/torbrowser/tor-browser-2.2.30-2-alpha_en-US.ex.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.30-2-alpha-osx-x86_64-en-US.zip" rel="nofollow">OS X 64-bit Tor Browser Bundle with Firefox 5</a> (<a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.30-2-alpha-osx-x86_64-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.30-2-alpha-osx-i386-en-US.zip" rel="nofollow">OS X 32-bit Tor Browser Bundle with Firefox 5</a> (<a href="https://torproject.org/dist/torbrowser/osx/TorBrowser-2.2.30-2-alpha-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.30-1-alpha-en-US.tar.gz" rel="nofollow">GNU/Linux 64-bit Tor Browser Bundle with Firefox 5</a> (<a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.30-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.30-1-alpha-en-US.tar.gz" rel="nofollow">GNU/Linux 32-bit Tor Browser Bundle with Firefox 5</a> (<a href="https://torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.30-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

