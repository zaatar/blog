title: Rompiendo barreras de censura, incluso cuando Tor está bloqueado
---
pub_date: 2018-06-22
---
author: antonela
---
tags:

bridges
censorship
censorship circumvention
venezuela
pluggable transports
---
categories: circumvention
---
summary: Mientras que el navegador Tor provee muchas propiedades de seguridad y privacidad, no todas las personas alrededor del mundo tienen el lujo de poder conectarse a la red para utilizarla.
---
_html_body:

<div id="magicdomid3"><span>[<a href="https://blog.torproject.org/breaking-through-censorship-barriers-even-when-tor-blocked">Post original</a>] </span></div>
<div> </div>
<div id="magicdomid4"><span>Mientras que el navegador Tor provee muchas propiedades de seguridad y privacidad, no todas las personas alrededor del mundo tienen el lujo de poder conectarse a la red para utilizarla. Por defecto, el navegador Tor hace que todos los usuarios conectados luzcan iguales, burlando a tu UserAgent (y otros métodos) para prevenir ataques de reconocimiento de huellas digitales. Sin embargo, no oculta el hecho de que estás conectado a la red Tor, una red abierta en la que todos pueden tener acceso a la lista de nodos (relays). Esta transparencia de la red tiene muchos beneficios, pero también cosas negativas: Muchos gobiernos represivos y autoridades gubernamentales se benefician al bloquear a los usuarios de tener un acceso a internet abierto y libre. Simplemente, toman la lista de relays y los bloquean. Esto impide que millones de personas accedan a información libre, incluso aquellos que más lo necesitan. A nosotros en Tor, nos preocupa la libertad de acceso a a información y nos oponemos a la censura firmemente. Por eso es que desarrollamos métodos para conectar a la red y saltar la censura. Este método se llama Transportes Intercambiables (<a href="https://www.torproject.org/docs/pluggable-transports">Pluggable Transports (PTs)</a>)</span></div>
<div id="magicdomid5"> </div>
<div id="magicdomid6"><span>Los Transportes Intercambiables son un tipo de puente para conectarse a la red Tor. Tienen la ventaja de que aprovechan varios transportes y hacen que el tráfico encriptado de Tor parezca tráfico poco interesante o basura. A diferencia de los relays normales, la información del puente se mantiene oculta y se distribuye entre los usuarios a través de <a href="https://bridges.torproject.org/">BridgeDB</a>. Si estás interesado en ayudar a los usuarios censurados, <a href="https://www.torproject.org/docs/pluggable-transports#operator">puedes convertirse en un operador puente</a>. Y si sos un desarrollador y tenés ideas interesantes sobre cómo hacer nuevos PT o contribuir con un código, <a href="https://www.torproject.org/docs/pluggable-transports#developer">tenemos documentos para ponerte al día</a>.</span></div>
<div id="magicdomid7"> </div>
<div id="magicdomid8"><span>Y finalmente, si sos un usuario censurado y querés usar los PT, tenemos buenas noticias. Los PT's ya están incluidos en el navegador Tor y este gráfico te muestra cómo hacerlo y te ayudará a configurarlo para eludir la censura. </span><span class="author-a-z86zz66zz75zz81zz73zlz85zz82zz88zloz70z1lrz86z">Para usuarios en Venezuela (y China), recomendamos seleccionar "meek-azure."</span></div>
<div> </div>
<div><img alt="Configure-Tor-sp" src="/static/images/blog/inline-images/tor-connect-sp.gif" class="align-center" /></div>
<div>Si meek-azure no funciona, escribinos a <a href="mailto:bridges@torproject.org">bridges@torproject.org</a> o visita <a href="https://bridges.torproject.org">bridges.torproject.org</a> para recibir puentes adicionales. Luego tienes que copiar y pegar la linea que contiene el bridge en Tor Browser Launcher.</div>
<div> </div>
<div id="magicdomid10"> </div>

---
_comments:

<a id="comment-275831"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275831" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Tony  (not verified)</span> said:</p>
      <p class="date-time">June 22, 2018</p>
    </div>
    <a href="#comment-275831">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275831" class="permalink" rel="bookmark">Excelente</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Excelente</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275836"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275836" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2018</p>
    </div>
    <a href="#comment-275836">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275836" class="permalink" rel="bookmark">Hey, gk! WTH is going on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey, gk! WTH is going on with memory consumption of Tor Browser on this page?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276036"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276036" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Venezolano (not verified)</span> said:</p>
      <p class="date-time">July 01, 2018</p>
    </div>
    <a href="#comment-276036">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276036" class="permalink" rel="bookmark">Tengo problemas al tratar de…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tengo problemas al tratar de acceder a TOR utilizando los DNS 1.1.1.1 y 1.0.0.1, no logro establecer una conexión. He tenido que regresar a los DNS de Google.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276046"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276046" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Vzla (not verified)</span> said:</p>
      <p class="date-time">July 02, 2018</p>
    </div>
    <a href="#comment-276046">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276046" class="permalink" rel="bookmark">meek-azure doesn&#039;t work in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>meek-azure doesn't work in Venezuela</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279095"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279095" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Johnny777 (not verified)</span> said:</p>
      <p class="date-time">December 17, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276046" class="permalink" rel="bookmark">meek-azure doesn&#039;t work in…</a> by <span>Vzla (not verified)</span></p>
    <a href="#comment-279095">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279095" class="permalink" rel="bookmark">Pero si funcionan los…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Pero si funcionan los bridges ! y perfectamente bien !</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-276087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276087" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>R (not verified)</span> said:</p>
      <p class="date-time">July 06, 2018</p>
    </div>
    <a href="#comment-276087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276087" class="permalink" rel="bookmark">que tipo de puente…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>que tipo de puente recomiendan para venezuela?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278782"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278782" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Isaac (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278782">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278782" class="permalink" rel="bookmark">Hello, good afternoon, I&#039;m…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, good afternoon, I'm following these steps but it does not connect me, I'm from Venezuela.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280097"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280097" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dnuke (not verified)</span> said:</p>
      <p class="date-time">March 01, 2019</p>
    </div>
    <a href="#comment-280097">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280097" class="permalink" rel="bookmark">I have just tested &quot;meek…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have just tested "meek-azure" in Venezuela, and it is not working here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280624"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280624" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>josedc (not verified)</span> said:</p>
      <p class="date-time">March 30, 2019</p>
    </div>
    <a href="#comment-280624">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280624" class="permalink" rel="bookmark">Hi, today 30/03/2019 any of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, today 30/03/2019 any of the built-in bridges works anymore in Venezuela :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-281640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281640" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ronin (not verified)</span> said:</p>
      <p class="date-time">May 11, 2019</p>
    </div>
    <a href="#comment-281640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281640" class="permalink" rel="bookmark">Hola buenos dias, es cierto…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hola buenos dias, es cierto meek-azure ya no funcione en venezuela</p>
</div>
  </div>
</article>
<!-- Comment END -->
