title: Help Censored Users, Run a Tor Bridge
---
pub_date: 2021-11-17
---
author: ggus
---
summary: Today the Tor Project is launching the Run a Tor Bridge campaign. Participants are invited to contribute and help censored users by running a Tor bridge. You can find more details below, including our special reward kits for participants. The campaign will run from November 17 to January 7, 2022. Join us and help censored users!
---
categories:

community
circumvention
announcements
relays
---
body:

## Run a Tor Bridge campaign

Bridges are private Tor relays that serve as stepping stones into the network. When the Tor network is blocked, users can get a bridge to circumvent censorship. Thanks to our community of bridge operators, users in China, Belarus, Iran, and Kazakhstan can connect to the Tor network and access the free and open Internet.

We currently have approximately 1,200 bridges, 900 of which support the obfs4 obfuscation protocol. Unfortunately, these numbers have been decreasing since the beginning of this year. It's not enough to have many bridges: eventually, all of them could find themselves in block lists. We therefore need a constant trickle of new bridges that aren't blocked anywhere yet. This is where we need your help.

![The Tor network size 2020 - 2021](/static/images/blog/inline-images/networksize-2020-01-01-2021-11-15.png)

The goal of this campaign is to bring more than **200 obfs4 bridges online** by the end of this year. We will wrap up the campaign on January 7, 2022. To show our appreciation for your volunteer work, we're offering unique and exclusive Tor reward kits. For example, if you run 10 obfs4 bridges for one year, you can get the Golden Gate bridge kit, including 1 Tor hoodie, 2 Tor T-shirts, and a sticker pack. Some of these kits are limited.

#### 1. Golden Gate bridge (limited to 10 kits)

 - Run 10 obfs4 bridges for 1 year.
 - Reward kit: 1 Tor hoodie + 2 Tor T-shirt + stickers pack.

#### 2. Helix bridge (limited to 20 kits)

 - Run 5 obfs4 bridges for 1 year.
 - Reward kit: 1 Tor T-shirt + stickers pack.

#### 3. University bridge kit (limited to 10 kits)

 - Run 2 obfs4 bridges for 1 year in your university.
 - Reward kit: 1 Tor T-shirt + stickers pack.

#### 4. Rialto bridge (randomly select 10 new bridge operator)

 - Run 1 obfs4 bridge for 1 year and you will be part of the 'reward lottery'.
 - We will randomly select 10 new bridge operators to receive a metallic roots Tor t-shirt as a token of our gratitude for your help defending the open internet.

![Bridge campaign rewards](/static/images/blog/inline-images/campaign-rewards-2021.png)

## Setting Up A Bridge

To set up an obfs4 bridge, check out our newly revised installation instructions. We have [guides](https://community.torproject.org/relay/setup/bridge/) for several Linux distributions, FreeBSD, OpenBSD, and docker. Note that an obfs4 bridge needs both an open OR *and* an open obfs4 port. If you run into any trouble while setting up your bridge, check out our [help page](https://community.torproject.org/relay/getting-help/) and [our new Forum](https://forum.torproject.net/c/support/relay-operator/).

Once you have set up your bridge, find your bridge line ([our post-install instructions explain how](https://community.torproject.org/relay/setup/bridge/post-install/)) and send an email to frontdesk@torproject.org. If you are running more than one bridge, it's preferable to send all of them once.

## Technical requirements

To join the bridges campaign, you must follow these requirements:

 - Static IPv4 address.
Although Tor bridges can operate behind dynamic IP addresses, this scenario is not that optimal while thinking about others who need to regularly configure the new IP addresses manually. IPv6 is definitely a plus, but it's not required.

 - Obfs4 pluggable transport configured. As this being the pluggable transport with higher probabilities of passing through global censorship, we opted to choose this one.

 - Uptime 24/7. Serving to the networking 24/7 is vital for bridges, as those who really need to workaround censorship depend on Tor being always available;

 - Only 2 bridges per IPv4 address.

 - Operators running more than 2 bridges should avoid sequential IP addresses. Sequential IP addresses make it easier for a group of bridges being blocked by entities that want filters against the whole network CIDR i.e. /24 for IPv4 or /64 for IPv6. If someone wants to block one bridge, and the whole netmask is used to block this particular bridge, and others are on the same address space, those other bridges will be blocked too, as a side effect.

 - Avoid running bridges on the same IP address of your (public) relay.

 - Relay operators running a large chunk of Tor exit nodes are discouraged to run bridges.

## Other campaign rules

 * Participants should claim their reward by commenting on the Bridge's topic on the Tor Forum and sending an email with their full bridge line to frontdesk@torproject.org.

 * Bridges will be tested and validated by the Tor Project staff.

 * Rewards for the **Golden Gate kit** will follow this timeline:
   * 1 month - Tor Stickers
   * 3 months - Tor T-shirt
   * 6 months - 2nd Tor T-shirt
   * 9 months - Hoodie

 * Rewards for **Helix kit** will follow this timeline:
   * 1 month - Tor Stickers
   * 5 months - Tor T-shirt

 * Bridge operators must follow the [Tor relay good practices](https://gitlab.torproject.org/tpo/community/relays/-/issues/18).

 * Due to our limited staff capacity at the end of year, Golden Gate and Helix operators should expect to receive their first reward in **January 2022**.

## Other ways to help

If you're not technical enough to run a bridge, but want to help censored users, there are other ways you can help:

* Run a Snowflake proxy. You do not need a dedicated server and can run a proxy by simply installing an extension in your browser. The extension is available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/) and [Chrome](https://chrome.google.com/webstore/detail/snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie). There is no need to worry about which websites people are accessing through your proxy. Their visible browsing IP address will match their Tor exit node, not yours.
* Make a [donation](https://donate.torproject.org) to the Tor Project to support our work developing and sharing tools for privacy and freedom online.
* [Help translate](https://community.torproject.org/localization) Tor materials and documentation including information on how to set up a bridge.
* Share your support of running and using Tor bridges on social media.
