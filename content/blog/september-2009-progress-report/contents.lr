title: September 2009 Progress Report
---
pub_date: 2009-10-10
---
author: phobos
---
tags:

china
bug fixes
censorship circumvention
iran
advocacy
performance improvements
senators
fbi
doj
---
categories:

advocacy
circumvention
global south
releases
---
_html_body:

<p>Here's what the Tor Project accomplished in September 2009.</p>

<p><strong>New Hires</strong></p>

<ul>
<li>Carolyn Anhalt is our new Translation and Community Manager. Carolyn has years of experience managing and growing content translation, as well as wrangling online communities and developing volunteer moderators and support roles from the community. She’s fluent or conversant in a number of languages, such as: Russian, French, English, German, Italian, and Welsh. Carolyn’s initial goals are to grow the translator community to keep everything Tor translated, work out better translation tools for translators, and to generally assist translators.</li>
<li>Karen Reilly joins us as our Development Director. Karen has years of experience in growing both community-based and foundation-based funding, as well as helping to fulfill the mission of organizations through outreach and community-building. Karen’s initial goals are to further develop community funding, work with our current donors, help create an annual report, and expand Tor’s outreach efforts.</li>
</ul>

<p><strong>New Funding</strong></p>

<ul>
<li>Tor and Drexel University receive a grant from the National Science Foundation to research “Privacy-preserving measurements of the Tor network to improve performance and  anonymity”.</li>
<li>Tor and ITT receive a grant from the Naval Research Laboratory to research “Tor Networks Trust-Based Routing Research &amp; Design Support”.</li>
</ul>

<p><strong>New Releases</strong></p>

<ul>
<li>On September 23rd, we released Tor version 0.2.2.3-alpha. <a href="https://blog.torproject.org/blog/tor-0223alpha-released" rel="nofollow">https://blog.torproject.org/blog/tor-0223alpha-released</a></li>
<li>On September 21, we released Tor version 0.2.2.2-alpha.  <a href="https://blog.torproject.org/blog/tor-0222alpha-released" rel="nofollow">https://blog.torproject.org/blog/tor-0222alpha-released</a></li>
<li>On September 7, we released Vidalia 0.2.4. <a href="https://blog.torproject.org/blog/vidalia-024-released" rel="nofollow">https://blog.torproject.org/blog/vidalia-024-released</a></li>
<li>On September 11, we released Tor Browser Bundle version 1.2.9.  <a href="https://blog.torproject.org/blog/tor-browser-bundle-129-released" rel="nofollow">https://blog.torproject.org/blog/tor-browser-bundle-129-released</a></li>
</ul>

<p><strong>Design, develop, and implement enhancements that make<br />
Tor a better tool for users in censored countries.</strong></p>

<ul>
<li>Jacob Appelbaum and Nathan Frietas developed a fully functional Tor for the Android mobile operating system. It is currently being tested before being released to the Android Marketplace. This Android application uses mainline Tor as written in C, rather than porting/creating a Tor client in Java. Others have started discussions and coding around using Java to create a Tor-compatible client.  A new mailing list was created to encourage this community to grow, <a href="http://archives.seul.org/tor/java/" rel="nofollow">http://archives.seul.org/tor/java/</a>.</li>
<li>Damian Johnson further developed arm, <a href="https://svn.torproject.org/svn/arm/trunk/" rel="nofollow">https://svn.torproject.org/svn/arm/trunk/</a>.  Arm is a command line application for monitoring Tor relays, providing real time status information such as the current configuration, bandwidth usage,  message log, connections, etc. This uses a curses interface much like ’top’ does for system usage. The application is intended for command-line aficionados, ssh connections, and anyone stuck with a tty terminal for checking their relay’s status.</li>
<li>Added two new Tor website and software distribution mirrors. Update list of current mirrors to reflect their current status of how current the mirrors are compared to the main torproject.org website.</li>
</ul>

<p><strong>Architecture and technical design docs for Tor enhancements<br />
related to blocking-resistance.</strong></p>

<ul>
<li>A paper entitled, “On the risks of serving whenever you surf: Vulnerabilities in Tor’s blocking resistance design”, discussing risks in running a bridge is to be relased at Workshop on Privacy in the Electronic Society (WPES 2009), <a href="http://freehaven.net/anonbib/#wpes09-bridge-attack" rel="nofollow">http://freehaven.net/anonbib/#wpes09-bridge-attack</a>.</li>
<li>Started development of “marco” to quickly scan for Tor relay and brige reachability from a client machine. This utility was used successfully by volunteers inside Iran and China to determine how much of the Tor network is blocked and how such blocking is occurring.</li>
</ul>

<p><strong>Outreach and Advocacy</strong></p>

<ul>
<li>Roger, Paul Syverson, and Andrew gave a Tor lecture to the US Federal Bureau of Investigation Operational Technology Division as part of their quarterly series of talks on new technology. Had a follow-on conversation with a Supervisory Special Agent about how to use Tor tools safely in the field.</li>
<li>Roger and Andrew had a meeting with the US Department of Justice Child Exploitation and Obscenity Section to present what Tor is and how it works. We also discussed issues and challenges in their usage of, and prosecuting criminals using, Tor.</li>
<li>Roger and Andrew met with a Senior Policy Advisor from Senator Harry Reid’s office to discuss online privacy and anonymity.</li>
<li>Roger is working with a freshman class at KAIST in South Korea to develop Tor bridge relay distribution strategies. More information can be found at <a href="https://blog.torproject.org/blog/bridge-distribution-strategies" rel="nofollow">https://blog.torproject.org/blog/bridge-distribution-strategies</a>.</li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<ul>
<li>On September 11, we released Tor Browser Bundle version 1.2.9.  <a href="https://blog.torproject.org/blog/tor-browser-bundle-129-released" rel="nofollow">https://blog.torproject.org/blog/tor-browser-bundle-129-released</a></li>
</ul>

<p><strong>Bridge relay and bridge authority work.</strong></p>

<ul>
<li>Deployed a temporary workaround for a vidalia/tor bug where bridges don’t work if you provide a fingerprint and the bridge authority is unreachable. Discovered this bug on September 25 when China blocked the bridge authority.</li>
<li>Started fixing bridge statistics that have been broken in all 0.2.2.x versions. Plan to test the fixes in the next few days and include the changes in 0.2.2.5-alpha.</li>
</ul>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<ul>
<li>A research paper on scaling our directory design, “Scalable onion routing with Torsk” is to be presented at CCS 2009, <a href="http://freehaven.net/anonbib#ccs09-torsk" rel="nofollow">http://freehaven.net/anonbib#ccs09-torsk</a>.</li>
<li>Deployed Mike Perry’s Bandwidth Authority code to the Directory Authorities. The bandwidth authorities are now voting on measured bandwidth from relays and giving out this information in extra-info fields to Tor clients. Tor 0.2.1 and 0.2.2 clients make routing decisions based on these extra-info data. This should spread traffic across relays and improve overall performance of the Tor network.</li>
<li>Included the results of Mike’s bandwidth scanner in the votes of gabelmoo. Helped evaluating the (impressive) performance improvements as seen by torperf, <a href="https://git.torproject.org/checkout/metrics/master/report/performance/torperf-2009-09-22.pdf" rel="nofollow">https://git.torproject.org/checkout/metrics/master/report/performance/t…</a>. Initial results imply a 30-50% increase in performance.</li>
<li> Evaluated how the reduction of circuit windows from 1000 to 101 cells affects performance. The last report includes 40 KiB, 50 KiB, and 1 MiB downloads, <a href="https://git.torproject.org/checkout/metrics/master/report/circwindow/circwindow-2009-09-20.pdf" rel="nofollow">https://git.torproject.org/checkout/metrics/master/report/circwindow/ci…</a>.</li>
<li>Evaluated how Mike’s buildtimes patch influences Tor performance, <a href="https://git.torproject.org/checkout/metrics/master/report/buildtimes/buildtimes-2009-09-22.pdf" rel="nofollow">https://git.torproject.org/checkout/metrics/master/report/buildtimes/bu…</a>.</li>
<li>Wrote a script that parses descriptor archives to tell whether an IP address was a Tor relay at a given time, <a href="https://svn.torproject.org/svn/projects/archives/trunk/exonerator/HOWTO" rel="nofollow">https://svn.torproject.org/svn/projects/archives/trunk/exonerator/HOWTO</a>.</li>
<li> Restarted the autonaming script on September 20, so that gabelmoo will be naming relays again soon.</li>
<li>Fixed the remaining bugs in the proposal 166 implementation. Relays can now include their statistics in extra-info descriptors. Further testing this on select relays for a few more days. Soon will ask people on or-dev to turn on statistics as soon as tests are successful and 0.2.2.4-alpha is out.</li>
</ul>

<p><strong>Translation work, ultimately a browser-based approach.</strong></p>

<ul>
<li>Released Runa’s code to allow website translation from our Translation Portal, <a href="https://translation.torproject.org" rel="nofollow">https://translation.torproject.org</a>. Runa wrote up her ideas and Google Summer of Code experiences at <a href="http://blog.torproject.org/blog/website-translation-support-translationtorprojectorg" rel="nofollow">http://blog.torproject.org/blog/website-translation-support-translation…</a>.</li>
<li>Continued work on website to translation portal code.</li>
<li>17 Polish website updates.</li>
<li>27 Italian website updates.</li>
<li>Romanian and Chinese updates to the check.torproject.org website.</li>
<li>1 update to German torbutton translation.</li>
<li>23 updates to Romanian torbutton translation.</li>
<li>115 updates to Polish torbutton translation.</li>
<li>115 updates to Mandarin Chinese torbutton translation.</li>
</ul>

---
_comments:

<a id="comment-2805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Luis (not verified)</span> said:</p>
      <p class="date-time">October 11, 2009</p>
    </div>
    <a href="#comment-2805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2805" class="permalink" rel="bookmark">I would like to participate</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to participate in the translation program helping Carolyn with Tor´s Portuguese version</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2808"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2808" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2805" class="permalink" rel="bookmark">I would like to participate</a> by <span>Luis (not verified)</span></p>
    <a href="#comment-2808">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2808" class="permalink" rel="bookmark">re: translations</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great!  you should email <a href="mailto:tor-translation@torproject.org" rel="nofollow">tor-translation@torproject.org</a> offering help.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2807"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2807" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2009</p>
    </div>
    <a href="#comment-2807">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2807" class="permalink" rel="bookmark">Impressive!
Harry Reid&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Impressive!</p>
<p>Harry Reid's office?  I never knew Tor was THAT well exposed.  Great work.  I generally worry about a Tor crackdown in the US.  Its great to hear about this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2882"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2882" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>keb (not verified)</span> said:</p>
      <p class="date-time">October 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2807" class="permalink" rel="bookmark">Impressive!
Harry Reid&#039;s</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2882">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2882" class="permalink" rel="bookmark">High level support and law enforcement usage is not enough</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If public opinion swings further toward domestic social control (or "harmony" as it is seen in China) and xenophobia, Tor usage could still be persecuted domestically.  Even though spooks and cops usage benefits from increased public adoption, they dont actually need Tor since they have pervasive monitoring tools and information sharing agreements with other governments.<br />
Imo, the direct education efforts being made seem most useful for preventing heavy handed tactics against Tor exit relays and operators.  And for development funding of course :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
