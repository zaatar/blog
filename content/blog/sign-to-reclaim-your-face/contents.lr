title: Sign now: European initiative for a ban on biometric mass surveillance
---
pub_date: 2021-03-17
---
author: matthiasmarx
---
summary: The “Reclaim Your Face” coalition has launched a European Citizens’ Initiative for a ban on biometric mass surveillance. Individuals can sign the petition for a ban on biometric mass surveillance. Organizations can get involved, too.
---
_html_body:

<div class="markdown-body container-fluid" id="doc">
<p>The “Reclaim Your Face” coalition has launched a European Citizens’ Initiative for a ban on biometric mass surveillance. European Digital Rights (EDRi) and more than fifty organizations are calling to sign the petition.</p>
<p>The initiative aims to put biometric mass surveillance on the political agenda in Brussels and stop the recent unregulated proliferation. European citizens can sign the petition at <a href="https://reclaimyourface.eu/" rel="noopener" target="_blank">https://reclaimyourface.eu</a>.</p>
<p>One million signatures must be collected in at least seven EU countries within one year. If this goal is to be reached, the European Commission must meet the organizers of the initiative. In addition, after a public hearing in the European Parliament, the Commission must explain its further course of action. In this way, the initiative wants to achieve a moratorium on all experiments with this high-risk technology.</p>
<h2 id="Illegal-experiments-on-humans">Illegal experiments on humans</h2>
<p>The time is pressing: we are already being abused as biometric guinea pigs. Shady companies like Clearview AI and pimeyes are building illegal face databases largely unchallenged. Some <a href="https://www.nature.com/articles/s41598-020-79310-1" rel="noopener" target="_blank">want to detect political attitudes</a> based on biometric data. Supposedly smart cameras are designed to read nonconforming and suspicious behavior from our movements to improve our “sense of security.” The European push-back agency <a href="https://corporateeurope.org/en/lobbying-fortress-europe" rel="noopener" target="_blank">Frontex would love to deploy a perverse bouquet of surveillance technologies</a>, including high-resolution cameras with facial, pattern and behavior recognition, with satellites and drones.</p>
<h2 id="Internet-style-tracking-in-the-offline-world">Internet-style tracking in the offline world</h2>
<p>Online, the surveillance industry keeps track of our every move. The resulting data can easily be stored and prepared for analysis. Luckily, we can use tools like Tor to protect our privacy. In the offline world, ubiquitous surveillance is not yet so easy: many camera systems are not networked and the insane amounts of data prevent mass surveillance ‒ but that’s changing fast. Biometric surveillance technology makes the networked, centralized, and all-encompassing recording of our offline everyday life a reality.</p>
<p>Online, we can keep pace with surveillance and meet new surveillance techniques with new defense measures. But the offline world is different: we cannot participate in everyday life through relays run by volunteers. We don’t all want to have to look the same to avoid being observed. We cannot hide from biometric mass surveillance.</p>
<h2 id="The-measured-body">The measured body</h2>
<p>Biometric data is particularly sensitive data about unchangeable characteristics of our bodies and behavior. They capture our faces, eyes, veins or voices, the way we walk or our typing on a keyboard. These “fingerprints” can often be used to identify people quickly and unambiguously. Like our fingerprints, we cannot simply change these characteristics. The methods used to process biometric data are often prone to error and discriminatory. On top of that, the way they work is often not comprehensible to third parties. Coupled with an unwarranted faith in technology, this poses immense risks and may even <a href="https://www.nytimes.com/2020/12/29/technology/facial-recognition-misidentify-jail.html" rel="noopener" target="_blank">put innocent people in jail</a>.</p>
<h2 id="Support">Support</h2>
<p><a href="https://reclaimyourface.eu/" rel="noopener" target="_blank">Individuals can sign the petition</a> for a ban on biometric mass surveillance. <a href="https://reclaimyourface.eu/wp-content/uploads/2021/02/Supporting-and-partnering-with-the-Reclaim-Your-Face-campaign_v3.pdf" rel="noopener" target="_blank">Organizations can get involved</a>, too.</p>
</div>

