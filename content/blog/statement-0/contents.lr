title: Statement
---
pub_date: 2016-07-27
---
author: ssteele
---
tags:

human resources
Jacob Appelbaum
---
categories: jobs
---
_html_body:

<p>Seven weeks ago, I published a blog post saying that Jacob Appelbaum had left the Tor Project, and I invited people to contact me as the Tor Project began an investigation into allegations regarding his behavior.</p>

<p>Since then, a number of people have come forward with first-person accounts and other information. The Tor Project hired a professional investigator, and she interviewed many individuals to determine the facts concerning the allegations. The investigator worked closely with me and our attorneys, helping us to understand the overall factual picture as it emerged.</p>

<p>The information shared was sensitive, and in writing this post I am aiming to balance my desire for the Tor Project to be transparent and accountable with my desire to respect individual privacy. </p>

<p>Here is what I am able to say:</p>

<p>The investigation is now complete. Many people inside and outside the Tor Project have reported incidents of being humiliated, intimidated, bullied and frightened by Jacob, and several experienced unwanted sexually aggressive behavior from him. Some of those incidents have been shared publicly, and some have not. The investigation also identified two additional people as having engaged in inappropriate conduct, and they are no longer involved with the Tor Project.</p>

<p>Based on the results of this investigation, we want to be more clear about (1) how we expect people to behave, (2) where people can take complaints and problems, (3) what will happen when complaints are received.</p>

<p>Putting procedures in place is more difficult for the Tor Project than for other organizations, because the staff of the Tor Project works in partnership with a broader Tor community, many of whom are volunteers or employed by other organizations. It is not a traditional top-down management environment. I am pleased, therefore, to announce that both the Tor Project and the Tor community are taking active steps to strengthen our ability to handle problems of unprofessional behavior. Specifically, the Tor Project has created an anti-harassment policy, a conflicts of interest policy, procedures for submitting complaints, and an internal complaint review process. They were recently approved by Tor’s board of directors, and they will be rolled out internally this week.</p>

<p>In addition, the Tor community has created a community council to help to resolve intra- community difficulties, and it is developing membership guidelines, a code of conduct, and a social contract that affirms our shared values and the behaviors we want to model. We expect these to be finalized and approved by the community at or before our next developer meeting at the end of September.</p>

<p>I believe these new policies and practices will make the Tor Project and the Tor community significantly healthier and stronger. I want to thank everyone who has contributed to the work we've done so far, and also to those who will contribute in the coming months.</p>

<p>I also want to note that the Tor Project board just elected a slate of new board members with significant governance and executive leadership experience. This was a bold and selfless decision by the outgoing board, to whom I am grateful. I am confident the new board will be a key source of support for the Tor Project going forward.</p>

<p>I want to thank all the people who broke the silence around Jacob's behavior. It is because of you that this issue has now been addressed. I am grateful you spoke up, and I acknowledge and appreciate your courage.</p>

<p>I look forward to instituting the changes described above and to continuing the Tor Project's important work.</p>

<p>--Shari Steele</p>

