title: Statement
---
pub_date: 2016-06-04
---
author: ssteele
---
tags:

ioerror
Jacob Appelbaum
---
_html_body:

<p>Over the past several days, a number of people have made serious, public allegations of sexual mistreatment by former Tor Project employee Jacob Appelbaum. </p>

<p>These types of allegations were not entirely new to everybody at Tor; they were consistent with rumors some of us had been hearing for some time. That said, the most recent allegations are much more serious and concrete than anything we had heard previously.</p>

<p>We are deeply troubled by these accounts. </p>

<p>We do not know exactly what happened here. We don't have all the facts, and we are undertaking several actions to determine them as best as possible. We're also not an investigatory body, and we are uncomfortable making judgments about people's private behaviors. </p>

<p>That said, after we talked with some of the complainants, and after extensive internal deliberation and discussion, Jacob stepped down from his position as an employee of The Tor Project.</p>

<p>We have been working with a legal firm that specializes in employment issues including sexual misconduct. They are advising us on how to handle this, and we intend to follow their advice. This will include investigations of specific allegations where that is possible. We don’t know yet where those investigations will lead or if other people involved with Tor are implicated. We will act as quickly as possible to accurately determine the facts as best we can. Out of respect for the individuals involved, we do not expect results to be made public.</p>

<p>People who have information to contribute are invited to contact me.  I will take input seriously, and I will respect its sensitivity.</p>

<p>People who believe they may have been victims of criminal behavior are advised to contact law enforcement. We recognize that many people in the information security and Internet freedom communities don't necessarily trust law enforcement. We encourage those people to seek advice from people they trust, and to do what they believe is best for them.</p>

<p>Going forward, we want the Tor community to be a place where all participants can feel safe and supported in their work. We are committed to doing better in the future. To that end, we will be working earnestly going forward to develop policies designed to set up best practices and to strengthen the health of the Tor community.</p>

<p>In our handling of this situation, we aim to balance between our desire to be transparent and accountable, and also to respect individual privacy. </p>

<p>We expect that this will be our only public statement.</p>

<p>Shari Steele<br />
Executive Director<br />
The Tor Project</p>

<p>Contact information:<br />
ssteele at torproject dot org<br />
pgp key:<br />
69B4 D9BE 2765 A81E 5736 8CD9 0904 1C77 C434 1056</p>

