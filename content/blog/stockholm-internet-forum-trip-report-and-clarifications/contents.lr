title: Stockholm Internet Forum Trip Report and Clarifications
---
pub_date: 2012-04-22
---
author: phobos
---
tags:

tor
community
sweden
anonymity online
stockholm internet forum
sida
misconceptions
onion routing
---
categories:

community
network
---
_html_body:

<h1>The quick trip report</h1>

<p>I spent the past week in Sweden for the Stockholm Internet Forum<a href="#fn1" rel="nofollow">1</a>, to meet up with our funders at Sida<a href="#fn2" rel="nofollow">2</a>, and to meet some activists looking for help and advice for their cause back in their home countries. Overall, it was a great trip. The Biståndsminister (Minister for Development)<a href="#fn3" rel="nofollow">3</a>, Gunilla Carlsson, specifically named Tor in her speech as a project she is proud to support and fund.<br />
In the afternoon, I gave a Tor talk to support DFRI <a href="#fn4" rel="nofollow">4</a>. The room was in a different building, way in back, with few signs to direct you to it. Hanna from dfri went out to grab people. In a short while, the room was packed, with people standing in the back and people sitting in the window seats. I would say roughly 35 people came and left during the session. I purposely did a quick 30 minute tor talk to leave time for questions. There were lots of questions, most about how to help and improve tor. The TeliaSonera<a href="#fn5" rel="nofollow">5</a> people were interested in the intersection of Tor and the EU Data Retention Directive being implemented in Sweden on May 1. I'm not sure if TeliaSonera is for or against data retention. Frank La Rue<a href="#fn6" rel="nofollow">6</a>, Special Rapporteur on the promotion and protection of the right to freedom of opinion and expression, was in the room for most of the talk too.</p>

<h1>Misconceptions around Tor</h1>

<p>Many Europeans thought we were a Swedish company already and were generally surprised to hear we are from the States. The Latin Americans and Middle East people are cautiously supportive of Tor. I kept running into misconceptions about Tor, the charity, the software, and if we're humans or not. Hopefully this post will clear up these misconceptions.</p>

<ol>
<li>Tor was not started by the US Navy. The US Naval Research Labs (NRL) started a project in the 1990s called onion routing<a href="#fn7" rel="nofollow">7</a>. Tor uses the basic onion routing principles and applies them to the Internet. The volunteer Tor group started in 2001. The formal charity, The Tor Project, started in 2006. We continue to work with Dr. Paul Syverson from NRL on improving onion routing and therefore Tor.</li>
<li>The goal of Tor is to give you control over your identity and privacy on the Internet. An equal goal is to enable research into anonymous communications on the Internet. We try very hard to make you anonymous by default. With this anonymity, it is up to you where you go, what you do, and what information about yourself you divulge. The goal is that you are in control.</li>
<li>In 2011, Tor received a total of $1.3 million in funding from a few sources: Internews, The Broadcasting Board of Governers, Sida, SRI International, and roughly 700 individual donors. Our forthcoming audit will show the funding and how we spent it. People seem to think Tor is a massive operation with hundreds of millions in funding. We publish our audit reports and financial statements every year after our audit is complete<a href="#fn8" rel="nofollow">8</a>.</li>
<li>Tor has a paid staff of 13 people. 10 of the 13 are developers and researchers. We have a part-time CFO, a marketing/policy person, and an Executive Director. We rely heavily on thousands of volunteers. We care a great deal about our community. Our core people<a href="#fn9" rel="nofollow">9</a> are the most dedicated to improving Tor and have contributed greatly to the cause. We are currently looking to make this 14 people by hiring a dedicated developer<a href="#fn10" rel="nofollow">10</a>.</li>
<li>We are human. Each of us involved is generally public about who we are and what we do for Tor. As we're only 13 people, we cannot be everywhere at once. We spend very, very little on marketing and advertising. A few of us, namely Roger, Jacob, Andrew, and Karen, do the bulk of public speaking. You can see various videos of our talks, lectures, and speeches in our media archive<a href="#fn11" rel="nofollow">11</a>.</li>
</ol>

<p>Overall, the trip to Sweden was successful. And I hope these five points clarify who and what is Tor.</p>

<ol>
<li><a href="http://www.stockholminternetforum.se/" rel="nofollow">http://www.stockholminternetforum.se/</a> <a href="#fnref1" rel="nofollow">↩</a></li>
<li><a href="http://www.sida.se/" rel="nofollow">http://www.sida.se/</a> <a href="#fnref2" rel="nofollow">↩</a></li>
<li><a href="http://www.regeringen.se/sb/d/7437" rel="nofollow">http://www.regeringen.se/sb/d/7437</a> <a href="#fnref3" rel="nofollow">↩</a></li>
<li><a href="https://www.dfri.se/" rel="nofollow">https://www.dfri.se/</a> <a href="#fnref4" rel="nofollow">↩</a></li>
<li><a href="http://www.teliasonera.com" rel="nofollow">http://www.teliasonera.com</a> <a href="#fnref5" rel="nofollow">↩</a></li>
<li><a href="http://www.ohchr.org/EN/Issues/FreedomOpinion/Pages/OpinionIndex.aspx" rel="nofollow">http://www.ohchr.org/EN/Issues/FreedomOpinion/Pages/OpinionIndex.aspx</a> <a href="#fnref6" rel="nofollow">↩</a></li>
<li><a href="http://www.onion-router.net/" rel="nofollow">http://www.onion-router.net/</a> <a href="#fnref7" rel="nofollow">↩</a></li>
<li><a href="https://www.torproject.org/about/financials.html.en" rel="nofollow">https://www.torproject.org/about/financials.html.en</a> <a href="#fnref8" rel="nofollow">↩</a></li>
<li><a href="https://www.torproject.org/about/corepeople.html.en" rel="nofollow">https://www.torproject.org/about/corepeople.html.en</a> <a href="#fnref9" rel="nofollow">↩</a></li>
<li><a href="https://www.torproject.org/about/jobs.html.en" rel="nofollow">https://www.torproject.org/about/jobs.html.en</a> <a href="#fnref10" rel="nofollow">↩</a></li>
<li><a href="https://media.torproject.org/video/" rel="nofollow">https://media.torproject.org/video/</a> <a href="#fnref11" rel="nofollow">↩</a></li>
</ol>

---
_comments:

<a id="comment-15136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 22, 2012</p>
    </div>
    <a href="#comment-15136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15136" class="permalink" rel="bookmark">is there any way to get work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is there any way to get work at the tor project?? are you guys employing right now? where do I send my resume??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2012</p>
    </div>
    <a href="#comment-15147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15147" class="permalink" rel="bookmark">I would not trust Telia they</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would not trust Telia they cooperate with the regimes doing the censorship and help to monitor citizens.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2012</p>
    </div>
    <a href="#comment-15151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15151" class="permalink" rel="bookmark">I am not currently a Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am not currently a Tor user. But applaud and appreciate what Tor is and does. Keep up the good work. (if this isnt the right place for this I apologize and feel free to laugh and or insult me.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 24, 2012</p>
    </div>
    <a href="#comment-15152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15152" class="permalink" rel="bookmark">In Vidalia Control Panel</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In Vidalia Control Panel under "about" - "license" it says the following:</p>
<p>"This product is produced independently from the Tor anonymity software and carries no guarantee from The Tor Project about quality, suitability or anything else."</p>
<p>Does this mean that noone from the Tor Project has audited the code for this software which comes with the tor browser bundle?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15619"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15619" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15152" class="permalink" rel="bookmark">In Vidalia Control Panel</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15619">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15619" class="permalink" rel="bookmark">This is indeed a disturbing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is indeed a disturbing question that demands an official response.</p>
<p>First, it is not even clear as to <i>which</i> product, specifically, the statement is referring-to; the text that precedes the disclaimer is divided into several sections, the headers of which name several distinct software products.</p>
<p>Beyond that, how can <i>any</i> integral part of the Tor Browser Bundle-- the flagship product of the Tor Project-- be "produced independently from the Tor anonymity software..." ?!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15172" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 25, 2012</p>
    </div>
    <a href="#comment-15172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15172" class="permalink" rel="bookmark">There are 13 people working</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are 13 people working for Tor .... and you can't even manage to launch a forum?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15620"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15620" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15172" class="permalink" rel="bookmark">There are 13 people working</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15620">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15620" class="permalink" rel="bookmark">What really seems to defy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What really seems to defy any logic or reason whatsoever is that there is no forum, yet the Tor Project somehow has the time and resouces to  not only offer free personal support by email but even personal support by <i>telephone</i> (practically unheard of for a free product).</p>
<p>How can this possibly be an efficient and cost-effective use of limited time and resources? </p>
<p>And instead of merely ignoring most of the questions that people post on the blog, why not at least place a disclaimer/statement somewhere where blog readers will see it, directing people where best to direct support inquiries?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15231"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15231" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 28, 2012</p>
    </div>
    <a href="#comment-15231">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15231" class="permalink" rel="bookmark">hello</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15436"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15436" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 05, 2012</p>
    </div>
    <a href="#comment-15436">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15436" class="permalink" rel="bookmark">is there any support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is there any support site/forum for torproject?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16322"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16322" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2012</p>
    </div>
    <a href="#comment-16322">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16322" class="permalink" rel="bookmark">my bandwidth is not same</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>my bandwidth is not same amount.<br />
Recv 600MB   sent 780MB</p>
<p>why??</p>
</div>
  </div>
</article>
<!-- Comment END -->
