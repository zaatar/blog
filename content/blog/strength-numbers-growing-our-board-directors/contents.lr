title: Strength in Numbers: Growing Our Board of Directors
---
pub_date: 2018-11-13
---
author: steph
---
tags:

Strength in Numbers
board of directors
---
summary: We are proud to welcome the newest member of our Board of Directors, Nighat Dad. Nighat is the founder and Executive Director of Digital Rights Foundation, Pakistan. She is an accomplished lawyer and human rights activist, and she is one of the pioneers campaigning for access to a safe and open internet in Pakistan.
---
_html_body:

<p><em>This is a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Please <a href="https://torproject.org/donate/donate-sinlbp2">contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p>Like most nonprofit organizations, the Tor Project relies on its Board of Directors to provide fiscal and corporate oversight to our important work. Over the past two years, the Tor Project has been focused on growing our board to reflect the diversity of cultures of people who build and use Tor.</p>
<p>We are proud to welcome the newest member of our Board of Directors, Nighat Dad. Nighat is the founder and Executive Director of Digital Rights Foundation, Pakistan. She is an accomplished lawyer and human rights activist, and she is one of the pioneers campaigning for access to a safe and open internet in Pakistan. <a href="https://www.ted.com/talks/nighat_dad_how_pakistani_women_are_taking_the_internet_back">Watch her TED talk</a> to hear the amazing story of how she set up Pakistan's first cyber harassment helpline to support women who face serious threats online--a major problem in Pakistan.</p>
<p>“Nighat brings an abundance of expertise and experience campaigning for digital rights in Pakistan and beyond,” said Isabela Bagueros, incoming Executive Director of the Tor Project. “She has strong ties to the communities we serve and our most at-risk users.”</p>
<p><a href="https://www.torproject.org/about/board.html.en">Our board</a> has eight members representing four continents: North America, Europe, Africa, and now Asia. Over the past year, the board held 16 official meetings plus several committee meetings during our searches for a new Executive Director and new board members.</p>
<p>In the coming year, we hope to continue to grow our board in number and in diversity. Like everyone involved with Tor, our Board of Directors all share a common commitment to internet freedom and human rights.</p>
<p>As we challenge major threats to internet freedom around the world, there is strength in numbers -- our numbers keep us strong as we challenge those threats. And our diversity gives us the understanding to fight with compassion.</p>
<p>Please join us to help us protect the privacy of millions of people online and ensure they have the right to express themselves and access critical resources without surveillance or censorship. <a href="https://torproject.org/donate/donate-sinlbp2">Donate today</a>.</p>
<p>As part of our year end fundraising campaign, Mozilla will be matching every dollar donated to the Tor Project, so your impact will be doubled. Every dollar counts twice.</p>
<p><a href="https://torproject.org/donate/donate-sinlbp2"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_1.png" class="align-center" /></a></p>
<p> </p>

---
_comments:

<a id="comment-278527"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278527" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278527">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278527" class="permalink" rel="bookmark">This is great news and a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is great news and a welcome development.  So many civil rights battles with international implications are being fought in Pakistan!</p>
<p>&gt; Our board has eight members representing four continents: North America, Europe, Africa, and now Asia. ...In the coming year, we hope to continue to grow our board in number and in diversity.</p>
<p>This is a very important goal and it is quite impressive to see how far TP has come in a few years.</p>
<p>Thanks to everyone who supports Tor and works to make TP stronger and more resistant to attempts by governments to outlaw the last vestiges of every individual freedom, in particular to outlaw the right to be secure in our personal communications and our access to information!</p>
</div>
  </div>
</article>
<!-- Comment END -->
