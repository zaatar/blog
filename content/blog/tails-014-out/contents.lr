title: Tails 0.14 is out!
---
pub_date: 2012-11-12
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.14, is out.</p>

<p>All users must upgrade as soon as possible.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p>Thank you, and congrats, to everyone who helped make this happen!</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Tor
<ul>
<li>Upgrade to 0.2.3.24-rc</li>
<li>Enable <a href="https://tails.boum.org/contribute/design/stream_isolation/" rel="nofollow">stream isolation</a></li>
</ul>
</li>
<li>Iceweasel
<ul>
<li>Upgrade iceweasel to 10.0.10esr-1+tails1, with anonymity enhancing patches from the TorBrowser applied</li>
<li>Fix Iceweasel's file associations. No more should you be suggested to open a PDF in the GIMP</li>
</ul>
</li>
<li>Hardware support
<ul>
<li>Upgrade Linux to 3.2.32</li>
<li>Support more than 4GB of RAM</li>
<li>Support more than one CPU core</li>
</ul>
</li>
<li>Miscellaneous
<ul>
<li>Mostly fix memory wiping at shutdown</li>
<li>gpgApplet can now handle public-key cryptography</li>
<li>Add a persistence preset for NetworkManager connections</li>
<li>Better support setting up persistence on large USB sticks</li>
<li>Make boot faster by fixing a read-ahead bug</li>
<li>Make shutdown faster by disabling useless scripts</li>
</ul>
</li>
<li>Localization
<ul>
<li>Custom software is now translated in many more languages</li>
<li>Add Japanese input system</li>
</ul>
</li>
</ul>

<p>Plus the usual bunch of bug reports and minor improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.14" rel="nofollow">online<br />
Changelog</a> for technical details.</p>

<p>Don't hesitate to <a href="https://tails.boum.org/support/" rel="nofollow">get in touch with us</a>.</p>

