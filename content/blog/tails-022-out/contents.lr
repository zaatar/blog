title: Tails 0.22 is out
---
pub_date: 2013-12-11
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.22, is out.</p>

<p>All users must upgrade as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.21/" rel="nofollow">numerous security issues</a>.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download</a> it now but first, please consider <a href="https://tails.boum.org/news/test_incremental_upgrades/" rel="nofollow">testing the incremental upgrade</a>.</p>

<p><strong>Changes</strong></p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade to Iceweasel 24.2.0esr that fixes a few serious security issues.</li>
<li>Stop migrating persistence configuration and access rights. Instead, disable all persistence configuration files if the mountpoint has wrong access rights.</li>
<li>Upgrade to NSS 3.15.3 that fixes a few serious security issues affecting the browser.</li>
</ul>
</li>
<li>Major improvements
<ul>
<li>Switch to Iceweasel 24.2.0esr and Torbutton 1.6.5.</li>
<li>Incremental upgrades are ready for beta-testing.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Fix Vidalia startup.</li>
<li>Disable DPMS screen blanking.</li>
<li>Fix checking of the persistent volume"s ACL.</li>
<li>Sanitize more IP and MAC addresses in bug reports.</li>
<li>Do not fail USB upgrade when the "tmp" directory exists on the destination device.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Clearer warning when deleting the persistent volume.</li>
<li>Use IBus instead of SCIM.</li>
<li>Always list optimal keyboard layout in the greeter.</li>
<li>Fix on-the-fly translation of the greeter in various languages.</li>
<li>Update I2P to 0.9.8.1 and rework its configuration.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li>The Unsafe Browser cannot connect to the Internet (<a href="https://labs.riseup.net/code/issues/6479" rel="nofollow">ticket #6479</a>). This can be workaround"ed by setting <span class="geshifilter"><code class="php geshifilter-php">network<span style="color: #339933;">.</span>proxy<span style="color: #339933;">.</span>socks_remote_dns</code></span> to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">false</span></code></span> on the <a rel="nofollow">about:config</a> web page.</li>
<li>Keyboard shortcuts use QWERTY mapping instead of AZERTY on French keyboard (<a href="https://labs.riseup.net/code/issues/6478" rel="nofollow">ticket #6478</a>). This may impact other keyboard layouts as well.</li>
<li>TorBrowser takes too long to shutdown (<a href="https://labs.riseup.net/code/issues/6480" rel="nofollow">ticket #6480</a>).</li>
<li>TorBrowser proposes to share the microphone with websites (<a href="https://labs.riseup.net/code/issues/6481" rel="nofollow">ticket #6481</a>).</li>
<li>htpdate uses a different User-Agent than the Tor Browser (<a href="https://labs.riseup.net/code/issues/6477" rel="nofollow">ticket #6477</a>).</li>
<li>The included Linux 3.10-3 (version 3.10.11-1) kernel has a few known security issues.</li>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page but first, please consider <a href="https://tails.boum.org/news/test_incremental_upgrades/" rel="nofollow">testing the incremental upgrade</a>.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://mailman.boum.org/pipermail/tails-dev/2013-December/004405.html" rel="nofollow">scheduled</a> for January 21.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

