title: Tails 1.2 is out
---
pub_date: 2014-10-16
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.2, is out.</p>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.1.2/" rel="nofollow">numerous security issues</a> and all users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Major new features
<ul>
<li>Install (most of) the Tor Browser, replacing our previous Iceweasel-based browser. The version installed is from TBB 4.0 and is based on Firefox 31.2.0esr. This fixes the POODLE vulnerability.</li>
<li>Upgrade Tor to 0.2.5.8-rc.</li>
<li>Confine several important applications with AppArmor.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Install Linux 3.16-3 (version 3.16.5-1).</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade I2P to 0.9.15, and isolate I2P traffic from the Tor Browser by adding a dedicated I2P Browser. Also, start I2P automatically upon network connection, when the <span class="geshifilter"><code class="php geshifilter-php">i2p</code></span> boot option is added.</li>
<li>Make it clear that <em>TrueCrypt</em> will be removed in Tails 1.2.1 (<a href="https://labs.riseup.net/code/issues/7739" rel="nofollow">ticket #7739</a>), and document how to open <em>TrueCrypt</em> volumes with <span class="geshifilter"><code class="php geshifilter-php">cryptsetup</code></span>.</li>
<li>Enable VirtualBox guest additions by default (<a href="https://labs.riseup.net/code/issues/5730" rel="nofollow">ticket #5730</a>). In particular this enables VirtualBox's display management service.</li>
<li>Make the OTR status in Pidgin clearer thanks to the formatting toolbar (<a href="https://labs.riseup.net/code/issues/7356" rel="nofollow">ticket #7356</a>).</li>
<li>Upgrade syslinux to 6.03-pre20, which should fix UEFI boot on some hardware.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li>Tor Browser is translated in <a href="https://tails.boum.org/support/known_issues#browser_languages" rel="nofollow">15 languages only</a>.
</li>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for November 25.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk<br />
to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

