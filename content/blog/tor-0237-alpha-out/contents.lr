title: Tor 0.2.3.7-alpha is out
---
pub_date: 2011-10-31
---
author: erinn
---
tags:

tor
bug fixes
alpha release
relays
---
categories:

network
relays
releases
---
_html_body:

<p>Tor 0.2.3.7-alpha fixes a crash bug in 0.2.3.6-alpha introduced by the new v3 handshake. It also resolves yet another bridge address enumeration issue.</p>

<p>All packages are updated, with the exception of the OS X PPC packages. The build machine is down and packages will be built as soon as it is back online.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>Changes in version 0.2.3.7-alpha - 2011-10-30<br />
<strong>Major bugfixes:</strong>
</p>

<ul>
<li>If we mark an OR connection for close based on a cell we process,<br />
      don't process any further cells on it. We already avoid further<br />
      reads on marked-for-close connections, but now we also discard the<br />
      cells we'd already read. Fixes bug 4299; bugfix on 0.2.0.10-alpha,<br />
      which was the first version where we might mark a connection for<br />
      close based on processing a cell on it.</li>
<li>Fix a double-free bug that would occur when we received an invalid<br />
      certificate in a CERT cell in the new v3 handshake. Fixes bug 4343;<br />
      bugfix on 0.2.3.6-alpha.</li>
<li>Bridges no longer include their address in NETINFO cells on outgoing<br />
      OR connections, to allow them to blend in better with clients.<br />
      Removes another avenue for enumerating bridges. Reported by<br />
      "troll_un". Fixes bug 4348; bugfix on 0.2.0.10-alpha, when NETINFO<br />
      cells were introduced.</li>
</ul>

<p><strong>Trivial fixes:</strong>
</p>

<ul>
<li>Fixed a typo in a hibernation-related log message. Fixes bug 4331;<br />
      bugfix on 0.2.2.23-alpha; found by "tmpname0901".</li>
</ul>

---
_comments:

<a id="comment-12430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2011</p>
    </div>
    <a href="#comment-12430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12430" class="permalink" rel="bookmark">Are you planning to release</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you planning to release unstable Tor Browser Bundles with the alpha version? Also, strangely Tor 0.2.2.x reports itself as an "experimental software" in Vidalia logs:</p>
<p>[Notice] Tor v0.2.2.34 (git-c4eae752f0d157ce). This is experimental software. Do not rely on it for strong anonymity. [...]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12439"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12439" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2011</p>
    </div>
    <a href="#comment-12439">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12439" class="permalink" rel="bookmark">Some downloads seems to be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some downloads seems to be corrupted or not properly managed by 7zip and dont open nor extract. Delete it and download again.It will works. Sugg: Unzip to USB or Pendrive. Be simple. A.P.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2011</p>
    </div>
    <a href="#comment-12460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12460" class="permalink" rel="bookmark">Tor 0.2.3.7 can be used in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor 0.2.3.7 can be used in China, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2011</p>
    </div>
    <a href="#comment-12469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12469" class="permalink" rel="bookmark">سلام من نمیتونم</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>سلام من نمیتونم تور را  راه بندازم  لطفا" تنظیمات آنرا  ارسال نمایید</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 01, 2011</p>
    </div>
    <a href="#comment-12470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12470" class="permalink" rel="bookmark">لطفا&quot; تنظیمات</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>لطفا" تنظیمات تور را برایم ارسال کنید<br />
<a href="mailto:atefehniaazi@yahoo.com" rel="nofollow">atefehniaazi@yahoo.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12484" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2011</p>
    </div>
    <a href="#comment-12484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12484" class="permalink" rel="bookmark">Newest release does not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Newest release does not support microdescriptors map in Vidalia.<br />
<a href="https://trac.torproject.org/projects/tor/ticket/4203" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/4203</a></p>
<p>What's wrong with you people? Such a bug and still no fix?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12524"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12524" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2011</p>
    </div>
    <a href="#comment-12524">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12524" class="permalink" rel="bookmark">What happened to the 1-click</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What happened to the 1-click tor button - they are 2 clicks for some time now?<br />
"Torbutton is a 1-click way for Firefox users to enable or disable the browser's use of Tor."<br />
<a href="https://www.torproject.org/torbutton/" rel="nofollow">https://www.torproject.org/torbutton/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
