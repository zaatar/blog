title: Tor Browser 6.0a3 is released
---
pub_date: 2016-03-08
---
author: boklm
---
tags:

tor browser
tbb
tbb-6.0
---
categories:

applications
releases
---
_html_body:

<p>A new alpha Tor Browser release is available for download in the <a href="https://dist.torproject.org/torbrowser/6.0a3/" rel="nofollow">6.0a3 distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha" rel="nofollow">alpha download page</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.7" rel="nofollow">security updates</a> to Firefox.</p>

<p>This release bumps the versions of several of our components, e.g.: Firefox to 38.7.0esr, Tor to 0.2.8.1-alpha, OpenSSL to 1.0.1s, NoScript to 2.9.0.4 and HTTPS-Everywhere to 5.1.4.</p>

<p>Additionally, we fixed long-standing bugs in our Tor circuit display and window resizing code, and improved the usability of our font fingerprinting defense further.</p>

<p>Here is the full changelog since 6.0a2:</p>

<p>Tor Browser 6.0a3 -- March 8</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 38.7.0esr</li>
<li>Update Tor to 0.2.8.1-alpha</li>
<li>Update OpenSSL to 1.0.1s</li>
<li>Update NoScript to 2.9.0.4</li>
<li>Update HTTPS Everywhere to 5.1.4</li>
<li>Update Torbutton to 1.9.5.1
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16990" rel="nofollow">Bug 16990</a>: Don't mishandle multiline commands</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18144" rel="nofollow">Bug 18144</a>: about:tor update arrow position is wrong</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16725" rel="nofollow">Bug 16725</a>: Allow resizing with non-default homepage</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16917" rel="nofollow">Bug 16917</a>: Allow users to more easily set a non-tor SSH proxy</li>
<li>Translation updates</li>
</ul>
</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18030" rel="nofollow">Bug 18030</a>: Isolate favicon requests on Page Info dialog</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18297" rel="nofollow">Bug 18297</a>: Use separate Noto JP,KR,SC,TC fonts</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18170" rel="nofollow">Bug 18170</a>: Make sure the homepage is shown after an update as well</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/16728" rel="nofollow">Bug 16728</a>: Add test cases for favicon isolation</li>
</ul>
</li>
<li>
  Windows
<ul>
<li>
     <a href="https://trac.torproject.org/projects/tor/ticket/18292" rel="nofollow">Bug 18292</a>: Disable staged updates on Windows
     </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-161285"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161285" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 08, 2016</p>
    </div>
    <a href="#comment-161285">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161285" class="permalink" rel="bookmark">tanks very very tank from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tanks very very tank from Iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-162486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161285" class="permalink" rel="bookmark">tanks very very tank from</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-162486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162486" class="permalink" rel="bookmark">hi I&#039;m sorry ro post here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi I'm sorry ro post here because I'm probably OT but I do t know where to ask for.belp<br />
after uodated my android with the new orbot app and the new browser orfox it looks like the browser is not worki lng anymore<br />
thenks for any belp and sorry for the OT</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-161804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161804" class="permalink" rel="bookmark">after attempting to update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>after attempting to update from 6.0a2 to 6.0a3, about:tor still says that my browser is out of date. I restarted after updating and also exited the browser and started it again with the same result.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161822"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161822" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161822">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161822" class="permalink" rel="bookmark">disregard my previous</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>disregard my previous comment. I finally got it to work, but it was a very strange ordeal. My processes tab in Task Manager showed the updater and tor.exe running as expected when I exited or tried to restart TorBrowser 6.0a3, however it would never actually restart or update. Everything seemed to run normally, then suddenly vanish. I had to directly open firefox.exe in TB's browser folder while the browser was still open and supposedly updating. It's difficult for me to describe exactly what happened and how I managed to get it to work, as I've never encountered this behavior before; so if anyone else experienced something similar, hopefully this helps you figure it out too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161823"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161823" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161823">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161823" class="permalink" rel="bookmark">same user with update issues</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>same user with update issues here<br />
lolI just glanced at the updates page and noticed <a href="https://trac.torproject.org/projects/tor/ticket/18292" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/18292</a>, which may have been what I was dealing with.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161964"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161964" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161964">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161964" class="permalink" rel="bookmark">Having trouble using Sync. </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Having trouble using Sync.  I have an account and I am signed in but Tor will not sync</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-161989"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-161989" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2016</p>
    </div>
    <a href="#comment-161989">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-161989" class="permalink" rel="bookmark">Won&#039;t play videos on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Won't play videos on <a href="https://www.youtube.com" rel="nofollow">https://www.youtube.com</a> with Privacy and Security settings on Low (default).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-212280"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-212280" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 05, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-161989" class="permalink" rel="bookmark">Won&#039;t play videos on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-212280">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-212280" class="permalink" rel="bookmark">I can&#039;t get YouTube vids to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't get YouTube vids to play either unless I have USB connection to desktop (with Tor on desktop too).  The videos load, show the play button, but won't actually play.<br />
I can go thru my regular browswer &amp; have YouTube play.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-162640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162640" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 13, 2016</p>
    </div>
    <a href="#comment-162640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162640" class="permalink" rel="bookmark">when will we get a stable</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>when will we get a stable tor browser bundle that uses firefox 45 ESR?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-162942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162942" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-162640" class="permalink" rel="bookmark">when will we get a stable</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-162942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162942" class="permalink" rel="bookmark">In June when ESR 38 will not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In June when ESR 38 will not be supported anymore: <a href="https://wiki.mozilla.org/RapidRelease/Calendar" rel="nofollow">https://wiki.mozilla.org/RapidRelease/Calendar</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-163700"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163700" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2016</p>
    </div>
    <a href="#comment-163700">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163700" class="permalink" rel="bookmark">Connecting multiple websites</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Connecting multiple websites nearly at the same time are nearly impossible for Tor Browser, if not allowed to do so, an adversary maybe easier to reveal  the  IP address by traffic measuring, supposed the adversary can see the traffic at both 2 ends.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-163825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-163825" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-163700" class="permalink" rel="bookmark">Connecting multiple websites</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-163825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-163825" class="permalink" rel="bookmark">Different websites can load</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Different websites can load through different circuits.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
