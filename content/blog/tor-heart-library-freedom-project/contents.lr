title: Tor at the Heart: Library Freedom Project
---
pub_date: 2016-12-07
---
author: alison
---
tags:

Library Freedom Project
heart of Internet freedom
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog8" rel="nofollow">Donate today</a></p>

<p><b>Library Freedom Project</b></p>

<p><a href="https://libraryfreedomproject.org" rel="nofollow">Library Freedom Project</a> is an initiative that aims to make real the promise of intellectual freedom in libraries by teaching librarians and their local communities about surveillance threats, privacy rights and responsibilities, and privacy-enhancing technologies to help safeguard digital freedoms. </p>

<p><b>Why libraries?</b></p>

<p>LFP focuses on libraries for several reasons: libraries are trusted community spaces and education centers, offering free computer classes and technology access -- quite often as the only such resource in their communities. Libraries serve people from all walks of life, including immigrants, poor and working people, and others who are under greater surveillance threats. Finally, libraries have a deep historical and ideological commitment to protecting privacy; for example, librarians in the United States <a href="https://www.thenation.com/article/librarians-versus-nsa/" rel="nofollow">were some of the earliest opponents </a>of overbroad government surveillance programs like the USA PATRIOT Act. Library Freedom Project helps librarians turn that ideological commitment into procedural and technical reality by learning to teach privacy classes, operate infrastructure for privacy-enhancing technologies, and understand what to do when faced with information requests for patron data.</p>

<p><b>LFP + Tor</b></p>

<p>Tor is an essential part of Library Freedom Project. Through privacy trainings, LFP has taught thousands of librarians about using and teaching Tor in their libraries. Dozens of these libraries have even installed Tor Browser on public computers or have started operating Tor relays to help protect privacy at home and across the world. The relationship between LFP and the Tor Project is mutually beneficial; the Tor Project builds a tool that librarians saw the need for years ago, and librarians have helped perform much needed outreach and training on behalf of Tor. Thanks to the work of LFP, Tor is well-recognized by librarians and fairly mainstream in library culture. It is not uncommon for a library conference to offer talks about using Tor in libraries, and <a href="http://www.theverge.com/2015/9/16/9341409/library-tor-encryption-privacy" rel="nofollow">LFP's Tor Relays in Libraries</a> project gave international attention to the role of libraries in the fight for privacy. </p>

<p><b>Support privacy training in your local community</b></p>

<p>By supporting Tor, you're helping bring privacy to local communities through the trusted space of the library. <a href="https://torproject.org/donate/donate-blog8" rel="nofollow">Donate to the Tor Project today</a>, and then tell your librarian about <a href="https://libraryfreedomproject.org" rel="nofollow">Library Freedom Project</a>.</p>

---
_comments:

<a id="comment-224408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224408" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224408" class="permalink" rel="bookmark">Thanks for this beautiful</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this beautiful summary! Hope more libraries join the battle, and maybe will even see an onion link for Archive.org who knows :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224412" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224408" class="permalink" rel="bookmark">Thanks for this beautiful</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224412" class="permalink" rel="bookmark">Onion Wayback Machine</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Onion Wayback Machine</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224415"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224415" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224408" class="permalink" rel="bookmark">Thanks for this beautiful</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224415">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224415" class="permalink" rel="bookmark">That&#039;s a great idea, and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's a great idea, and given the Internet Archive's historic support for Tor, I think they'd be into it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224544"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224544" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-224544">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224544" class="permalink" rel="bookmark">wouldnt a way back machine</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>wouldnt a way back machine for onion sites make onion services more dangerous and turn people away from using them. i mean people who use onionshare for one time only .onion addresses and other sensitive use cases like that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224687"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224687" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224544" class="permalink" rel="bookmark">wouldnt a way back machine</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224687">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224687" class="permalink" rel="bookmark">The original comment was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The original comment was only talking about a .onion address to access the existing Wayback Machine, not necessarily caching .onion sites within the Wayback Machine. If the latter was implied, it could make .onion sites slightly more dangerous, but not to a greater extent than it already does for clearnet sites. It uses its own user agent ID, and respects robots.txt, so sites are free to block it if they wish. Also note that it is very likely well within the reach of various governments and even private companies to crawl and cache the entire (known) .onion space fairly regularly (cf. NSA's Utah 1YB data center for example).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224544" class="permalink" rel="bookmark">wouldnt a way back machine</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225066" class="permalink" rel="bookmark">Huh, too late! &quot;Wayback</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Huh, too late! "Wayback Machine has started adding onion sites to the Internet Archive, says director @MarkGraham" <a href="https://twitter.com/xor/status/807327064718077952" rel="nofollow">https://twitter.com/xor/status/807327064718077952</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-224411"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224411" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224411">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224411" class="permalink" rel="bookmark">I&#039;ve actually thought about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've actually thought about contacting my library about running a relay before, but I didn't know where to start or how to "sell" the idea. Little did I know, it must be a fairly common thing.</p>
<p>Does LFP provide a sort of "tutorial on introducing your local library to Tor" or anything like that? To be honest, I've never even been to my city's library (always used universities' and nearby towns'), and I certainly don't know how to prepare for asking them for permission to setup a relay, or install TB on their computers, or host informational campaigns about internet privacy. E.g. example letters, presentations, or testimonials from other libraries could be helpful.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224418" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224411" class="permalink" rel="bookmark">I&#039;ve actually thought about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224418" class="permalink" rel="bookmark">The best way to start is to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The best way to start is to introduce them to Library Freedom Project and see if they want to take one of our privacy trainings. Here's an intro pdf you can send them: <a href="https://libraryfreedomproject.org/wp-content/uploads/2015/03/join-LFP.pdf" rel="nofollow">https://libraryfreedomproject.org/wp-content/uploads/2015/03/join-LFP.p…</a>. It can be hard for a member of the public to convince them to run relays or install TB, but we know how to make the case. You can also show them some of the good press we've received: libraryfreedomproject.org/press.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224661"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224661" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-224661">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224661" class="permalink" rel="bookmark">Thanks for the links.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the links. They'll be a big help. One of the biggest problems I can think of at the moment is that, if running an exit relay, the whole network will be blocked by many sites, or will trigger additional verification when signing in, unless the library has a public IP to dedicate to the relay (doubtful). Even the exit only uses IPv6 on the outbound side, it will likely cause the whole /48 to be blocked.</p>
<p>Sure, this would make a great campaign for convincing sites not to block Tor, but I can't expect any library or it's customers to deal with the inconvenices on a daily basis. Is there any solution to this other than running a non-exit relay?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225126"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225126" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224661" class="permalink" rel="bookmark">Thanks for the links.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225126">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225126" class="permalink" rel="bookmark">I agree, this would be a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree, this would be a huge problem to introduce to a library. That's why we only set up relays at libraries where they are able to dedicate an IP to the relay. We would never want to interfere with the network that patrons are using.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-224430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224430" class="permalink" rel="bookmark">Why is this organization a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is this organization a Limited Liability Company (LLC) instead of a 501(c)(3)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224452"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224452" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224430" class="permalink" rel="bookmark">Why is this organization a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224452">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224452" class="permalink" rel="bookmark">Because in the US, if you&#039;re</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Because in the US, if you're a tiny organization that can't support the requirements of a 501(c)3 on your own, you need something called a fiscal sponsorship. In order to get a fiscal sponsorship, you need to be an LLC. LFP is an LLC with a fiscal sponsor who provides us the use of their 501(c)3 status for seeking grants and things like that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224856"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224856" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-224856">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224856" class="permalink" rel="bookmark">There are so many reasons</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are so many reasons for seriously considering moving Tor Project outside the USA.  And US laws are rapidly changing, often stealthily, in ways which are extremely inimical for freedom of speech and access to good information.  </p>
<p>Unfortunately, much the same can be said of most other countries, so it may be hard to identify a truly suitable home country for Tor Project and for key employees.  Germany is not ideal but right now certainly seems a better choice than the USA.  Let's meet up in Berlin!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-224493"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224493" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224493">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224493" class="permalink" rel="bookmark">the library freedom project</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the library freedom project ? us product for us guys.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224580"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224580" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224493" class="permalink" rel="bookmark">the library freedom project</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224580">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224580" class="permalink" rel="bookmark">I don&#039;t know what this means!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't know what this means!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224685"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224685" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-224685">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224685" class="permalink" rel="bookmark">cool !
i am speaking about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>cool !<br />
i am speaking about the location of the library project ( i am almost certain that there are in the usa except few in south america) not about the relays of tor or the users or their mission - good luck -</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224770"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224770" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224685" class="permalink" rel="bookmark">cool !
i am speaking about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224770">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224770" class="permalink" rel="bookmark">I don&#039;t think the physical</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think the physical location of LFP is that significant, except maybe for financial reasons. All it takes is for someone in South Africa to begin working with libraries in their area, someone like you! See the links in the blog post, and the reply to my earlier comment about introducing your local library to Tor, for more information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224818"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224818" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224770" class="permalink" rel="bookmark">I don&#039;t think the physical</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224818">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224818" class="permalink" rel="bookmark">exactly! thanks for this.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>exactly! thanks for this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-224686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224686" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-224686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224686" class="permalink" rel="bookmark">Just my interpretation, but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just my interpretation, but I think the OP meant "U.S." in reference to the project being based in America, although I don't see why it couldn't be useful to libraries all around the world. Maybe they only accept donations from the U.S., or there are no translations available for non-english readers?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224783"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224783" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224686" class="permalink" rel="bookmark">Just my interpretation, but</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224783">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224783" class="permalink" rel="bookmark">&gt; the project being based in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; the project being based in America ONLY<br />
&lt; that's the point !<br />
the library freedom project is a wonderful &amp; fantastic dream and i do hope that the donations will support it at the end of this year and even after.<br />
unfortunately afaik the project runs in english_spanish &amp; is not located all around the world.<br />
(it should fail in a lot of place _oppression - extortion - etc. = us product for us guys = chain of trust = democracy ! )</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224819"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224819" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224783" class="permalink" rel="bookmark">&gt; the project being based in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224819">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224819" class="permalink" rel="bookmark">You&#039;re right, we are limited</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You're right, we are limited in our reach because we have a tiny staff (we do have Farsi too, not just English and Spanish, but your point is well taken). However, we can provide some help for folks who want to do this in their own region too!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224940"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224940" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224783" class="permalink" rel="bookmark">&gt; the project being based in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224940">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224940" class="permalink" rel="bookmark">Translations of the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Translations of the materials would help, but as for presence, the way is see it is it's not LFP's responsibility (or even feasible) to put representives in many countries. It's your responsibility to step up and represent LFP in your country and work with your local library.</p>
<p>That's the way LFP is intended to work. For example, LFP may be present in the U.S., but I will be the first and probably only person representing LFP in my area and working with local libraries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225002"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225002" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224940" class="permalink" rel="bookmark">Translations of the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225002">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225002" class="permalink" rel="bookmark">&gt; That&#039;s the way LFP is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; That's the way LFP is intended to work : only in democracy not inside the european union.<br />
Here (in the country where i live) it is impossible _ a local library (some are private &amp; could do it but are not able to and do not wish it &amp; are most often a second police office than a free area) is a  reserved/controlled zone.<br />
Uk &amp; german countries are better for this project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225127" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224940" class="permalink" rel="bookmark">Translations of the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225127" class="permalink" rel="bookmark">I&#039;m psyched that you want to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm psyched that you want to work with LFP in your area! And you are more than welcome to use our materials (CC-BY-SA licensed) for your own presentations. We do ask that you not use the logo or name (eg marketing something as an LFP training) without our expressed consent though. If you want to work on something together, please contact us at info (at) libraryfreedomproject (dot) org. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div><a id="comment-224859"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224859" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
    <a href="#comment-224859">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224859" class="permalink" rel="bookmark">Thanks so much for this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks so much for this wonderful program!</p>
<p>I hope many Tor users will print out this blog and send it with a cover letter to their local libraries (public, university, institutional*) asking them to participate in this vital project.</p>
<p>*"Institutional": large law firms, medical schools, major tech companies, and national laboratories usually maintain research libraries!  Recent news stories show very clearly that such information resources are exactly the kind which are attacked by rival national intelligence services for many reasons.  And the most secure way to access a library catalog appears to be by using an onion service.  So it is in the best interests of the "national security" apparatus, as well as legal, financial and health-care institutions, as well as public schools, to set up onion services for better cybersecurity.</p>
<p>Let's put an onion in every school library!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225128"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225128" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224859" class="permalink" rel="bookmark">Thanks so much for this</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225128">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225128" class="permalink" rel="bookmark">Thank you! You can find an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! You can find an intro letter to use here: libraryfreedomproject.org/resources. Onions in every library!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
