title: Tor Metrics - Google Summer of Code 2010
---
pub_date: 2010-10-14
---
author: kjbbb
---
tags:

gsoc
metrics
gsoc2010
---
categories:

internships
metrics
---
_html_body:

<p>Although it has been a while since GSoC ended, I want to give a quick run-down of my project and how everything went over the summer. The plan was to migrate the <a href="https://metrics.torproject.org/index.html" rel="nofollow">Tor metrics portal</a> from the file based system to a database driven one. This allows us to have a more dynamic website and a better interface to derive our statistics and visuals from. I spent most of my time designing the database schema (which had to be carefully optimized due to the amount of data we have), and the remainder of the time working on things such as <a href="https://metrics.torproject.org/graphs.html" rel="nofollow">dynamic graphs</a> and migrating the website to Java Server Pages. Thankfully, I finished almost everything I had planned to do, and it seems as though my work this summer is being put to good use! A significant amount of my metrics code has been merged, which is certainly a good feeling. </p>

<p>While I did not have any specific research questions in mind, I was able to uncover some interesting things by exploring the wealth of data in front of me. One of the things I promised to deliver was relay churn - a statistic to show the stability of the network. The Y axis, or the "churn ratio" is rather abstract, but it tells us the percentage of unique router fingerprints from one cohort that appear in the following cohort. Unfortunately, it requires a significant amount of database power to produce (large table scans are necessary when grouping routers by time period - the smaller the interval the longer the operation takes). However, the logic is clear and is easily accomplished from the database. The churn graphs may find their way onto the website, but the underlying processes may need to be optimized further. Either way, a database system ought to open a few doors for us in regards to some of the interesting things we can easily explore. See some of the churn graphs <a href="https://blog.torproject.org/static/images/blog/inline-images/relay-churn-week.png" rel="nofollow">here</a>, and <a href="https://blog.torproject.org/static/images/blog/inline-images/relay-churn-month.png" rel="nofollow">here</a>.<br />
In addition to the back-end and database work, I explored <a href="http://had.co.nz/ggplot2/" rel="nofollow">ggplot2</a> and the <a href="http://www.r-project.org/" rel="nofollow">R</a> language - the tools used to generate the graphs on the metrics portal. Currently, R communicates with the database to generate graphs on demand (I settled on a <a href="http://code.google.com/p/rpostgresql/" rel="nofollow">Postgres driver</a> for R which was part of GSoC 2008). The goal was to make the graphs parametizable so a user can request a custom or unique graph. The graphs are then served from <a href="http://rosuda.org/Rserve/" rel="nofollow">Rserve</a> and Apache Tomcat. A few examples of the parametizable graphs are currently live on the metrics website which were implemented by Karsten.<br />
Congratulations to the rest of the GSoC'ers John, Kory, and Harry for their successful projects. I want to give a big thank you to @KarstenLoesing for being such a great guide for my foray into the Tor Project and open-source development. It has been a great learning experience. Many thanks to Tor and Google for the opportunity!</p>

---
_comments:

<a id="comment-8137"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8137" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 14, 2010</p>
    </div>
    <a href="#comment-8137">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8137" class="permalink" rel="bookmark">Hello,
what about a network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>what about a network card with tor on a lower layer and with a chip or something?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2010</p>
    </div>
    <a href="#comment-8152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8152" class="permalink" rel="bookmark">Frankly, TOR is useless</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Frankly, TOR is useless since by now many websites are employing measures against it. RIP TOR. </p>
<p>It was a nice experiment, though.</p>
<p>And it is still a great way to catch stupid criminals who think they can hide behind TOR. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8163"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8163" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2010</p>
    </div>
    <a href="#comment-8163">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8163" class="permalink" rel="bookmark">IM install this program
1</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>IM install this program<br />
1 dont work in IEXLPLORER<br />
2 dont work in firefox (and can not install "buttonx"<br />
3 dont work in others program thats uses socks5<br />
4 polipo dont work...<br />
this program make looss my time .. really i hate the guys that create incomplete programs .,, very bad..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2010</p>
    </div>
    <a href="#comment-8165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8165" class="permalink" rel="bookmark">Where can I get the TOR</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can I get the TOR alpha builds?</p>
<p>I guess it is missing from the new website</p>
<p>Chris</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8256" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2010</p>
    </div>
    <a href="#comment-8256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8256" class="permalink" rel="bookmark">It&#039;s Tor not TOR damnit! ;p</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's Tor not TOR damnit! ;p</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8321"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8321" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2010</p>
    </div>
    <a href="#comment-8321">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8321" class="permalink" rel="bookmark">I haven&#039;t used TOR so can I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I haven't used TOR so can I get more details about it? Are you sure it is useless?<br />
Please make it clear to me....<br />
If you need any help regarding "Sales territory mapping" then we will be glad to serve you...<br />
<a href="http://www.indicia-geomarketing.com" rel="nofollow">http://www.indicia-geomarketing.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
