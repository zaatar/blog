title: Tor Weekly News — May 14th, 2014
---
pub_date: 2014-05-14
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the nineteenth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tor Browser 3.6.1 is released</h1>

<p>On May 7th, <a href="https://blog.torproject.org/blog/tor-browser-361-released" rel="nofollow">version 3.6.1 of the Tor Browser was released</a>. Apart from updating HTTPS Everywhere and NoScript, the new release mainly solves a <a href="https://trac.torproject.org/projects/tor/ticket/11658" rel="nofollow">regression experienced by proxy users</a>.</p>

<p>The new version should not error out with “You have configured more than one proxy type” anymore.</p>

<h1>More monthly status reports for April 2014</h1>

<p>More monthly reports from Tor project members have arrived this week with submissions from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000531.html" rel="nofollow">Nicolas Vigier</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000533.html" rel="nofollow">Roger Dingledine</a>.</p>

<p>Roger also sent the report for <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000532.html" rel="nofollow">SponsorF</a>. The <a href="https://tails.boum.org/news/report_2014_04/" rel="nofollow">Tails team has released theirs</a>.</p>

<h1>Miscellaneous news</h1>

<p><a href="https://lists.torproject.org/pipermail/ooni-dev/2014-May/000114.html" rel="nofollow">ooniprobe 1.0.2 has been released</a>. The new version brings security fixes, a manpage, a test for Tor bridge reachability among other improvements.</p>

<p>As the Tor blog should <a href="https://bugs.torproject.org/10022" rel="nofollow">migrate away from its current decaying software</a>, Eric Schaefer <a href="https://lists.torproject.org/pipermail/www-team/2014-May/000316.html" rel="nofollow">wrote</a> to tell that he had extracted all blog posts in a format ready for a static site generator. Comments are also available. One option would be to import them in a dedicated commenting system. Tom Purl has <a href="https://lists.torproject.org/pipermail/www-team/2014-May/000318.html" rel="nofollow">setup</a> a test Juvia instance for anyone who wish to give it a shot.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-qa/2014-May/000410.html" rel="nofollow">released</a> a new round of Tor Browser packages modified to include <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a>. “Unlike previous bundles […], these ones aren’t configured to use meek automatically. You have to select ‘Configure’ on the network settings screen and then choose meek from the list of transports.” Please give them a try!</p>

<p>Isis Lovecruft <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006856.html" rel="nofollow">rewrote</a> the email bridge distributor in order to fix some fundamental design problems with the old code. Reviews are welcome.</p>

<h1>Tor help desk roundup</h1>

<p>A relay operator contacted the Tor Help Desk after seeing the following message in the Tor log: “http status 400 ("Fingerprint is marked rejected") response from dirserver '128.31.0.34:9131'”.</p>

<p>One might see this message is if one’s relay was found to be vulnerable to the Heartbleed OpenSSL bug and subsequently removed from the Tor consensus. <a href="https://blog.torproject.org/blog/openssl-bug-cve-2014-0160" rel="nofollow">Instructions for upgrading one’s relay</a> are on the Tor project’s blog. </p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan, Karsten Loesing and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

