title: Tor Weekly News — May 14th, 2015
---
pub_date: 2015-05-14
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the nineteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.7.1-alpha is out</h1>

<p>Nick Mathewson <a href="https://blog.torproject.org/blog/tor-0271-alpha-released" rel="nofollow">announced</a> the first alpha release in the Tor 0.2.7.x series. The most notable new feature in this release is the ability to create, delete, control, and get information about onion services and their descriptors via a Tor controller. It also fixes a bug that assigned the HSDir flag to relays that would not function properly as directories, affecting the availability of some onion services over the past few weeks.</p>

<p>The source code is available as usual from the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>; as with any alpha release, “please expect bugs”.</p>

<h1>Tails 1.4 is out</h1>

<p>The Tails team <a href="https://blog.torproject.org/blog/tails-14-out" rel="nofollow">announced</a> version 1.4 of the anonymous live operating system. Most notable in this release is the inclusion of Tor Browser 4.5, meaning Tails users can now take advantage of the security slider, enhanced anti-tracking protection, and other exciting new features in the latest stable Tor Browser series.</p>

<p>Another user-facing enhancement is the addition of <a href="http://www.jabberwocky.com/software/paperkey/" rel="nofollow">Paperkey</a>, a program that lets you back up the secret part of your GPG key on the one storage medium that never goes out of fashion.</p>

<p>For a full list of changes, see the team’s announcement. This release contains important security updates, so head to the <a href="https://tails.boum.org/download" rel="nofollow">download page</a> (or the incremental updater) as soon as possible.</p>

<h1>Tor Cloud is retired</h1>

<p>The <a href="https://cloud.torproject.org/" rel="nofollow">Tor Cloud project</a>, which offered prospective Tor relay operators an easy way of setting up a bridge relay on Amazon’s EC2 cloud computing platform, has been discontinued, as Karsten Loesing explained on the <a href="https://blog.torproject.org/blog/tor-cloud-service-ending-many-ways-remain-help-users-access-uncensored-internet#comments" rel="nofollow">Tor blog</a>.</p>

<p>“The main reason for discontinuing Tor Cloud is the fact that software requires maintenance, and Tor Cloud is no exception”, wrote Karsten. Several serious bugs have rendered Tor Cloud unusable, and no solution could be found for its continued maintenance, so the service will no longer be offered for new relays.</p>

<p>This doesn’t mean, however, that existing Tor Cloud relays will be shut down as well — those will continue to run as long as their operators want them to. Similarly, everyday Tor Browser users do not need to worry about this announcement: it will have no effect on the working of software that uses the Tor network.</p>

<p>If you want to help grow the Tor network and strengthen the protection it offers to Internet users around the world, but don’t have the resources to set up a relay from scratch, please consider donating to a Tor relay organization like <a href="https://www.torservers.net/donate.html" rel="nofollow">Torservers.net</a>, <a href="https://nos-oignons.net/Donnez/index.fr.html" rel="nofollow">Nos Oignons</a>, or another of their <a href="https://www.torservers.net/partners.html" rel="nofollow">partners</a>. These experienced Tor relay operators will run the services on your behalf, ensuring they stay secure, efficient, and up-to-date. Please see the organizations’ donation pages for more details!</p>

<h1>Relay operators: please enable IPv6!</h1>

<p>“We still have a depressingly low number of relays that support IPv6 (currently only ~120 of ~1900 relays)”, wrote Moritz Bartl in a post to the <a href="https://lists.torproject.org/pipermail/tor-relays/2015-May/006964.html" rel="nofollow">tor-relays mailing list</a>. If your host supports the new protocol, enabling it on your Tor relay is as simple as a change to your torrc file: please see Moritz’ post for the full details.</p>

<h1>More monthly status reports for April 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of April continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000821.html" rel="nofollow">Karsten Loesing</a> (coordinating translations and reports for SponsorO, researching onion service statistics, and developing Onionoo), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000822.html" rel="nofollow">Noel Torres</a> (responding to Tor help desk requests), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000824.html" rel="nofollow">Sukhbir Singh</a> (developing Tor Messenger), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000825.html" rel="nofollow">Isabela Bagueros</a> (overall project management), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000826.html" rel="nofollow">Arlo Breault</a> (also working on Tor Messenger).</p>

<p>David Goulet sent out the report for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000823.html" rel="nofollow">SponsorR team</a>, who are researching Tor onion services and working on improvements to their security and stability.</p>

<h1>Miscellaneous news</h1>

<p>meejah <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008810.html" rel="nofollow">announced</a> version 0.13.0 of txtorcon, the Twisted-based asynchronous Tor controller. This version brings with it speed improvements, as well as support for “basic” and “stealth” onion service authentication; see meejah’s announcement for full details.</p>

<p>The Tails team published their monthly report for <a href="https://tails.boum.org/news/report_2015_04" rel="nofollow">April 2015</a>. Take a look for news of recent development work, summaries of ongoing discussions, upcoming events, and more.</p>

<p>Lunar gave an interview (in French) to <a href="https://lundi.am/Tor-Loi-Renseignement" rel="nofollow">lundimatin</a> on the subject of the French government’s “<a href="https://en.wikipedia.org/wiki/Bill_on_Intelligence" rel="nofollow">Bill on Intelligence</a>” and what it means for the Tor network.</p>

<p>Jacob Appelbaum took part in a <a href="https://voicerepublic.com/talks/a-deeper-frontier-of-freedom-the-state-of-the-deepweb" rel="nofollow">panel discussion</a> at re:publica 15 entitled “A Deeper Frontier of Freedom: The State of the Deepweb”.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, the Tails team, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

