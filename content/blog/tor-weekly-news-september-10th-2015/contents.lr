title: Tor Weekly News — September 10th, 2015
---
pub_date: 2015-09-10
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-fifth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Introducing the tor-teachers list</h1>

<p>Just as the the Tor network itself grows and evolves through the efforts of volunteer relay operators in numerous countries,  information about how and why users should make use of the protections that Tor offers is also spread by an informal network of teachers and activists working in many different communities around the world. Tor talks and trainings are often a feature of free public privacy events like cryptoparties, as well as Internet security workshops put on by groups and organizations especially in need of online privacy in their activities.</p>

<p>Until now, Tor teachers have had no central meeting-place to share advice, compare experiences, or make future plans, so Alison Macrina and Nima Fatemi this week <a href="https://lists.torproject.org/pipermail/tor-talk/2015-September/038933.html" rel="nofollow">announced</a> the creation of the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-teachers" rel="nofollow">tor-teachers mailing list</a>. According to Alison, whose <a href="https://libraryfreedomproject.org/" rel="nofollow">Library Freedom Project</a> is itself engaged in teaching Tor and other online privacy tools to librarians and library patrons across America (<a href="http://www.libfocus.com/2015/08/reflections-on-online-privacy.html" rel="nofollow">and beyond</a>), “this list is for all the awesome people around the world who are teaching Tor to their communities, who want to work collectively with other teachers of Tor to support each other, build community, and make our work even better”. Topics of discussion will range from “visionary stuff” like the philosophical underpinnings of the right to free expression and inquiry, to more prosaic Tor-related questions such as “how to use the darn thing” and how best to convey this to users from all backgrounds.</p>

<p>If this sounds like the sort of thing you either would like to be doing or are already an old hand at, you are most welcome to join! Visit the list-info page to sign up. As with almost all of Tor’s mailing lists, messages are publicly visible and archived, so you can take a look at current discussions to see if you want to get involved. Good luck!</p>

<h1>Miscellaneous news</h1>

<p>Luke Millanta announced the launch of <a href="https://onionview.com" rel="nofollow">OnionView</a>, a web service which utilizes Tor relay data, gathered using the <a href="https://onionoo.torproject.org/" rel="nofollow">Onionoo</a> network status protocol, to plot the location of active Tor nodes onto an interactive map of the world. Created in collaboration with Tor’s Measurement team, OnionView’s relay database is updated every thirty minutes to help ensure map accuracy. Join the developers in the #tor-dev IRC channel to become involved in future work on OnionView.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and Luke Millanta.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

