title: Website translation support for translation.torproject.org
---
pub_date: 2009-09-20
---
author: Runa
---
tags:

gsoc
translation
website
---
categories:

internships
localization
---
_html_body:

<p>This summer I was working on a set of scripts that would make it possible to translate the <a href="https://www.torproject.org/" rel="nofollow">Tor Project website</a> via <a href="http://translate.sourceforge.net/wiki/pootle" rel="nofollow">Pootle</a> on <a href="http://translation.torproject.org" rel="nofollow">translation.torproject.org</a>. The website is based on a set of .wml files, but Pootle only takes files in the .pot and po format. The goal was to find a solution that would make it easy to not only translate from .wml to .po and back, but enable us to convert and keep the already translated documents.</p>

<p>The solution that I chose was to use the <a href="http://po4a.alioth.debian.org/" rel="nofollow">po4a framework</a> in a set of scripts. The first script, <a href="https://tor-svn.freehaven.net/svn/website/trunk/wml2po.sh" rel="nofollow">wml2po.sh</a>, converts from .wml to .po and adds the correct charset, encoding and copyright holder. The script also checks to see which .po files need to be updated and which don't. The second script, <a href="https://tor-svn.freehaven.net/svn/website/trunk/po2wml.sh" rel="nofollow">po2wml.sh</a> writes the translated documents, i.e. converts from .po back to .wml. I also documented the things one would need to know before using the scripts, as well as the process of converting already translated documents. See the <a href="https://tor-svn.freehaven.net/svn/translation/trunk/tools/gsoc09/HOWTO" rel="nofollow">howto</a> and the <a href="https://tor-svn.freehaven.net/svn/translation/trunk/tools/gsoc09/README" rel="nofollow">readme</a>.</p>

<p>The project did not come without its challenges, and it took time before I got on the right track and found a solution that everyone was happy with. I looked at different scripts and SVN hooks before I settled on having two scripts that can be run at any time by anyone. In addition to that, my mentor was travelling a lot and communication was not very frequent. However, I do feel that I learned a lot and, at the same time, the communication with other GSoC students was a great help.</p>

<p>Overall, I'm pleased with how GSoC turned out. I am looking forward to seeing my code be put to use and to start working on other projects for Tor.</p>

---
_comments:

<a id="comment-4091"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4091" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2010</p>
    </div>
    <a href="#comment-4091">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4091" class="permalink" rel="bookmark">Interesting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting project.currently i am very busy but want to take part in this project in the future. good luck!!</p>
<p>___________<br />
Omidiu, part of the <a href="http://www.traduceriautorizate.biz" rel="nofollow">Traduceri</a> team.</p>
</div>
  </div>
</article>
<!-- Comment END -->
