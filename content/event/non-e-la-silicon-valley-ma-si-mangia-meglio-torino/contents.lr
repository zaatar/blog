title: Non è la silicon valley, ma si mangia meglio! (Torino)
---
author: ggus
---
start_date: 2019-11-19
---
end_date: 2019-11-21
---
body:

![Non è la silicon valley, ma si mangia meglio!](/sites/default/files/inline-images/gancio-non-e-la-silicon-valley-torino-2019.jpg)

Tre giorni di incontri e dibattiti (con tanti contenuti) per esplorare insieme l'ecosistema digitale.

Proveremo, insieme, ad andare a fondo su temi come la sicurezza, la privacy e l'ecologia. Spesso ci sembrano parole vuote e prive di significato dato che vengono quotidianamente abusate perchè accattivanti e alla moda.

Non ci vogliamo accontentare di riempirci soltanto la bocca con queste parole, ma vogliamo capirle: comprendere cosa si portano dietro e quali implicazioni hanno per noi; indagare la relazione tra questi concetti, le nostre vite, quello che studiamo e che ci piace.  

Ci fanno orrore le macchine chiuse, per questo non vogliamo dare solo delle risposte, ma discuterne assieme e confrontarci per aprirle e vedere come funzionano!

**Talks**
----------

Tor - Privacy e Anonimato  

by Gustavo Gus  

19/11 h16-18 - aula D

Scopriamo Tor, uno strumento per poter navigare in modo sicuro ed anonimo, usato tutti i giorni per usare la rete in sicurezza combattendo il tracciamento e la censura. Ne Parleremo con il leader del community team del Tor Project.

Autodifesa Digitale  

by Hacklab Underscore Torino  

20/11 h16-18 - aula B

Vi portiamo a spasso per un giretto dietro le quinte della società del controllo, provando con mano vie di fuga digitali e tecniche di autodifesa.

Non forniremo soluzioni facili o magiche, non siamo venditori di pentole, toccherà accendere il cervello, respirare, sbagliare, darsi il tempo di capire, portarsi i compiti a casa.

Daremo un'infarinatura su queste tematiche a nostro avviso sempre più importanti nel contesto di merda in cui ci troviamo, dove gli ingredienti saranno paranoia quanto basta e uno spazio conviviale dove poter riprendere consapevolezza degli strumenti digitali, cercando di usarli evitando di fare male a noi stessi e ai nostri simili.

Materialità del Digitale  

by Le Dita nella Presa da Radio Onda Rossa  

h16-17.30 21/11 - aula B

Si dice che la digitalizzazione inquina meno della carta. Sara' vero? Ripercorriamo il ciclo di vita di un oggetto elettronico e vediamo l'inquinamento che c'e' dietro.

Abstract pi`u lungo:

Si dice che la digitalizzazione inquina meno della carta. Sara' vero?  

Tiriamo fuori il nostro approccio hacker (cioe' curioso e irrispettoso delle verita' calate dall'alto) e andiamo a vedere quanto c'e' di vero in questo luogo comune.  

Usiamo la metodologia della Life Cycle Analysis, quindi consideriamo gli oggetti tecnologici in maniera integrata con la societa' in tutte le loro fasi: da dove viene la materia prima per fare i telefoni? chi li produce? e quando non funzionano piu' che fine fanno?  

E se volessimo fare un dispositivo piu` sostenibile, come potremmo fare?

Il dibattito parte da una serie di trasmissioni di approfondimento andate in onda su Radio OndaRossa nell'ultimo anno.

Hacker Cabaret  

by StakkaStakka da Radio Black Out  

21/11 h18.30-19 - aula B

Grandi sorprese e ricchi premi!

Website:
[Non è la silicon valley, ma si mangia meglio!](https://gancio.cisti.org/event/563)
